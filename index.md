---
title: Avant-propos
layout: cover
---
Mémoire d'[Antoine Fauchié](https://www.quaternum.net/) dans le cadre du Master Sciences de l'information et des bibliothèques, spécialité Publication numérique, de l'[Enssib](http://enssib.fr), sous la direction d'[Anthony Masure](http://www.anthonymasure.com/), maître de conférences en design à l’université Toulouse – Jean Jaurès et de [Marcello Vitali-Rosati](http://vitalirosati.com/), professeur au département des littératures de langue française de l'Université de Montréal et titulaire de la Chaire de recherche du Canada sur les écritures numériques.

{{ site.version}} – {{ site.versiondate }} – [source]({{ site.repo_url }})

### Résumé
{{ site.resume }}

#### Mots-clés
{{ site.mots-cles }}

### Abstract
{{ site.abstract }}

#### Keywords
{{ site.keywords }}


### Imprimer ce site web
[Version PDF imprimable](/telechargement/fauchie-antoine-vers-un-systeme-modulaire-de-publication-cc-by-nc-sa.pdf).

### Contexte
Ce travail de recherche est réalisé dans le cadre d'une Validation des acquis de l'expérience (VAE) en Master Sciences de l'information et des bibliothèques, spécialité Publication numérique à l'[Enssib](http://www.enssib.fr/).
