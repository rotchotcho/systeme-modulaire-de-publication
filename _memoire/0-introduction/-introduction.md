Manipuler, ouvrir et lire un livre sont des actes qui engendrent des sensations et des expériences multiples.
L'une d'entre elles, probablement la plus inconsciente, concerne la matérialité du livre, qu'il soit imprimé ou numérique : le codex ou l'_ebook_ renferme l'aboutissement de plusieurs dizaines de siècles de recherche et de développement.
Ouvrir un livre est donc l'occasion de prendre la mesure de cette prouesse technologique que sont quelques cahiers reliés ou quelques fichiers compressés.
Le livre est un moyen d'interroger le rapport que nous entretenons avec la technique, la façon de concevoir et de fabriquer une publication est révélatrice de notre positionnement avec les machines et les programmes.
L'informatisation des métiers du livre ainsi que l'arrivée du livre numérique et des nouveaux modes de diffusion de l'écrit modifient la manière d'élaborer des livres ou plus globalement des publications.
Conception, fabrication, production, diffusion : l'influence du _numérique_ sur le livre nous amène à devoir nous questionner sur la chaîne de publication et sur ce qui la compose.
D'un modèle linéaire nous pouvons envisager un système modulaire, et ainsi remettre en cause des approches basées sur des logiciels.
En d'autres termes, comment éditer _avec_ le numérique[^faire-avec] ?

## Informatisation des métiers du livre
Les métiers du livre – auteurs, édition, distribution/diffusion, librairie et bibliothèque – connaissent différentes phases d'informatisation depuis les années 1970, portées par un marché économique en transformation {% cite longhi__2014 %}.
Que ce soit les systèmes d'information des bibliothèques, puis les logiciels de traitement de texte et de publication assistée par ordinateur des auteurs et des éditeurs, et les infrastructures de diffusion de l'information pour la distribution/diffusion et la librairie, les activités des métiers du livre ont été bouleversées avec la même force que l'arrivée de l'imprimerie à caractères mobiles.
Les bibliothèques ont été pionnières avec la mise en place de catalogues permettant de gérer le classement et l'organisation des collections ; les maisons d'édition, quant à elles, ont commencé à utiliser des ordinateurs et des logiciels à partir du début des années 1980.
Les premiers traitements de texte et logiciels de publication assistée par ordinateur ont créé de nouvelles possibilités pour les éditeurs et les auteurs : temps de conception réduit, facilité de modification des maquettes, prévisualisation des compositions et mutualisation des efforts avec des schémas reproductibles.
Aujourd'hui deux logiciels semblent disposer d'une position hégémonique : le traitement de texte Microsoft Word et le logiciel de publication assistée par ordinateur Adobe InDesign sont les deux outils professionnels les plus répandus dans le domaine de la publication, qu'il s'agisse d'imprimé ou de numérique.
Leurs fonctionnalités correspondent à des besoins identifiés à travers le prisme de l'objet imprimé, le livre numérique numérique est l'occasion de réviser le paradigme imposé par ces logiciels.

## Essor du livre numérique
Le livre numérique est une vieille histoire : le premier specimen a été créé en 1971 par Michael Hart dans un objectif de diffusion d'un texte majeur via les premiers réseaux informatiques {% cite lebert_livre_2016 -L chapter -l 1 %}.
La version numérique de _The Declaration of Independence of the United States of America_ par Thomas Jefferson, au format texte et sans mise en forme, est également le point de départ du projet Gutenberg ([http://www.gutenberg.org/ebooks/1](http://www.gutenberg.org/ebooks/1)).
Ce n'est qu'à la moitié des années 2000 que le livre numérique est popularisé : avec la standardisation du format EPUB en 2015, la disponibilité d'un support de lecture à encre électronique bon marché – aussi appelé liseuse – en 2016, et enfin avec la commercialisation d'un catalogue numérique également en 2016.

Le livre numérique reste majoritairement homothétique – les enrichissements sont rares {% cite benhamou_livre_2014 -l 148 %} –, et il est souvent produit par un sous-traitant après la version imprimée, générant potentiellement une dette technique {% cite faucilhon_production_2016 %}.
Si l'_ebook_ représente une nouvelle économie fragile – la part des ventes numériques dans le chiffre d'affaires des éditeurs peine à atteindre les 10% {% cite noauthor_les_2018 %} –, il ébranle toutefois les principes de l'édition.
Le contenu liquide peut être reformaté par le lecteur, remettant en cause les choix typographiques de l'éditeur, comme le souligne Philippe Millot :

>Avec le texte au format numérique, à la mise en page liquide, chacun peut changer les paramètres élémentaires.
Il faudrait être typographe soi-même pour lire son livre savamment et ne pas jeter plus de mille ans de connaissance de mise en forme du texte.  
{% cite renou-nativel_les_2012 %}

Aussi, la dimension de flux concerne autant le livre que sa diffusion sur Internet sur des plates-formes diverses, et la fabrication des EPUB repose sur des technologies empruntées au Web – notamment les langages HTML et CSS {% cite vitali_rosati_pratiques_2014 %}.
Au-delà des nouvelles possibilités de diffusion il y a en creux des changements plus profonds, comme la manière de concevoir un livre : le texte n'est plus forcément composé page à page, mais doit être considéré comme un flux {% cite tangaro_page_2017 %} dont certains comportements peuvent être anticipés.
Par ailleurs l'utilisation de formats ouverts et de standards qu'implique l'EPUB invite ceux qui fabriquent les livres à envisager de nouvelles façons de faire et ainsi à modifier leur chaîne de publication.

## Chaîne de publication
Par chaîne de publication nous entendons l'ensemble des méthodes et des outils permettant de concevoir, fabriquer, produire et diffuser un livre ou une publication.
Une chaîne de publication réunit des logiciels propres à la gestion du texte et des images, mais aussi les différents rôles d'un processus éditorial que sont l'auteur, l'éditeur, le correcteur, le graphiste ou l'imprimeur.
Une chaîne de publication, également appelée chaîne d'édition ou chaîne éditoriale {% cite arribe_conception_2014 -l 206 %}, a pour objectif de gérer des contenus depuis le manuscrit d’un auteur jusqu'à la publication imprimée ou numérique d'un ouvrage, en passant par les phases de structuration, de relecture et de mise en forme.
Cette chaîne, ce _workflow_, est le noyau d’une maison d’édition pour manier des textes et leur donner vie sous forme de livre.
Classiquement une chaîne de publication comprend un traitement de texte et un logiciel de publication assistée par ordinateur, certains domaines éditoriaux comme l'édition scientifique ont des outils pour structurer plus finement le contenu et pour générer des formats spécifiques.
Identifier les éléments d'une chaîne d'édition est une étape nécessaire pour pouvoir la reconsidérer et ainsi formuler un nouveau modèle.

## Interroger la façon de produire des livres
L'informatisation des métiers du livre et le développement du livre numérique sont deux phénomènes qui conduisent à repenser la manière de fabriquer des publications : il ne s'agit plus seulement de réaliser des ouvrages imprimés avec des logiciels et de créer des équivalents _dématérialisés_, mais de prendre en compte la dimension numérique dès la conception et la fabrication, et jusque dans les formes proposées.
Si les outils utilisés dans l'édition sont vus comme de simples exécutants de tâches précises et complexes, l'_ebook_ a introduit une nouvelle dimension, invitant celui ou celle qui compose le livre à entrer au cœur du contenu, et à concevoir de nouvelles formes.

**Si l'informatique a longtemps été considérée comme au service d'un objet imprimé, comment, aujourd'hui, éditer _avec_ le numérique ?**

**Si le modèle linéaire et relativement figé des chaînes d'édition classiques est pertinent pour la conception et la production d'ouvrages imprimés, ne faut-il pas le revisiter pour des publications numériques en s'inspirant des méthodes et des technologies utilisées dans le développement web ?**

L'un des objectifs de cette conceptualisation d'un projet non conventionnel est de donner une plus grande maîtrise à celles et ceux qui produisent des textes et des publications : auteurs et éditeurs en premier lieu, notamment dans le domaine de l'édition savante {% cite vitali-rosati_edition_2016 %}, mais plus globalement pour les ouvrages dits de _non-fiction_ comme les essais ou les manuels.
Et il y a urgence.

## Avec le numérique
Afin d'appréhender un nouveau modèle de fabrication des livres ou des publications, ce mémoire s'articule autour de trois grandes parties qui constituent également une méthodologie à trois pans : l'analyse de trois textes, l'analyse de trois initiatives originales et la formalisation d'une proposition inédite.

Avant de découvrir des cas dans lesquels le numérique prend une place prépondérante pour réaliser des publications, nous devons exposer les hybridations de l'imprimé et du numérique, critiquer un usage solutionniste de la technologie et présenter un nouveau rapport à la technique.
Cette première partie constitue le premier volet de notre méthodologie, et entend nourrir les propositions de la troisième partie.
L'ouvrage d'Alessandro Ludovico, _Post-Digital Print : la mutation de l'édition depuis 1894_, explore les nombreux exemples en œuvre depuis la fin du dix-neuvième siècle en matière d'édition _hybride_.
_Pour tout résoudre, cliquez ici : l’aberration du solutionnisme technologique_ d'Evgeny Morozov met à mal les recours parfois hâtifs de la technologie pour améliorer le monde.
Enfin, la thèse de Gilbert Simondon publiée en 1958, _Du mode d'existence des objets techniques_, ouvre une nouvelle perspective quant au positionnement de l'homme par rapport aux machines – et par extension, aux programmes informatiques.

Le numérique est bien souvent une version supplémentaire venant s'ajouter au traditionnel ouvrage papier, plutôt qu'une nouvelle dimension autant pour l'accès au texte que pour sa production.
Certaines structures d'édition ont fait évoluer leur chaîne pour prendre en compte d'abord la version numérique, se passant du même coup de logiciels dits classiques pour l'élaboration d'ouvrages imprimés.
Cette seconde partie, également second versant de notre méthodologie, est un outil indispensable pour parvenir à extraire les éléments d'un nouvel archétype.
Nous analysons les approches de trois structures d'édition pour exposer et comprendre leur fonctionnement : les différents outils développés par l'éditeur O'Reilly Media, la chaîne de publication nativement numérique Quire de Getty Publications, et la revue académique _Distill_.

Dans un troisième temps, et à partir des textes étudiés et des cas analysés, nous esquissons les principes d'un modèle alternatif : un système modulaire de publication.
Nous décortiquons tout d'abord les différentes étapes qui composent une chaîne d'édition : écriture, structuration, validation, mise en forme et génération des formats.
Nous exposons ensuite trois principes à partir desquels un système modulaire de publication peut être mis en place : interopérabilité, modularité et multiformité.
En filigrane et à partir des analyses de texte, nous interrogeons notre rapport à la technique.

> La technique est un bien social à investir d’urgence et à tout prix. […]  
> Qu’elle soit un langage, un objet ou un milieu, la technique est un bien social parce qu’elle est l’une des composantes indispensables à l’existence et à l’évolution des liens sociaux.  
> {% cite gelgon_dialogue_2018 %}

## Un mémoire modulaire
La construction de ce mémoire est progressive et originale : à la suite d'un article introductif publié en 2017 {% cite fauchie_chaine_2017 %}, les différentes parties ont été rédigées afin de pouvoir être lues indépendamment.
Les commentaires de texte et les analyses de cas, traditionnellement placés en annexe, font partie intégrante du mémoire.
Celui-ci est un agencement de textes autonomes suivant un fil rouge commun, plusieurs sens de lecture sont possibles.

Ce mémoire est l'occasion d'appliquer, pour cette modeste publication, les principes exposés ici : l'interopérabilité, la modularité et la multiformité.
Les sources sont dans un format interopérable et sont versionnées, permettant d'envisager différents traitements du texte, mais aussi pour faciliter les éventuelles interventions extérieures[^memoire-depot].
Ce texte est disponible en version numérique, et plus particulièrement sous la forme d'un site web, mais également en version imprimée[^version-imprimee].

Enfin, un certain nombre de travaux ont été réalisés en parallèle de ce mémoire – articles, communications lors de colloques et collaborations diverses : ces initiatives satellites permettent de structurer et d'alimenter un travail de recherche qui a débuté en mars 2017, ouvrant également des opportunités de collaboration avec des personnes qui produisent des publications _avec_ le numérique[^annexe-satellites].

La lecture de _Vers un système modulaire de publication : éditer avec le numérique_ est plurielle : elle peut débuter par la partie 3 pour ensuite revenir aux parties 2 et 1, tout comme elle peut être linéaire en suivant l'ordre du plan.
Si les processus, quels qu'ils soient, doivent toujours être interrogés, il en va de même de ce mémoire qui est une incitation à reconsidérer les modes de fabrication du livre plutôt qu'une proposition à appliquer.
Enfin, cet exercice de recherche est une tentative de publier _avec_ le numérique.

[^faire-avec]: Le titre de cette introduction, "Faire avec le numérique", est une référence inconsciente au premier numéro de la revue _Back Office_, intitulé "Faire avec" (http://www.revue-backoffice.com/numeros/01-faire-avec).
[^memoire-depot]: Un dépôt Git public permet à quiconque de contribuer : https://gitlab.com/antoinentl/systeme-modulaire-de-publication/.
[^version-imprimee]: Une version PDF imprimable de ce mémoire est disponible en annexe.
[^annexe-satellites]: Une annexe est dédiée aux différentes réalisations en marge de ce mémoire, avec une bibliographie dédiée.
