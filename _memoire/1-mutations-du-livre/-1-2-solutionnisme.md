Evgeny Morozov est un chercheur et écrivain d'origine biélorusse, dont l'objet de recherche principal est le "problème technologique"[^silicon-circus].
Collaborateur de plusieurs revues et auteur pour des journaux comme le New York Times ou El Pais, il a enseigné à l'Université Stanford et il est membre d'un certain nombre de fondations engagées dans la critique de la technologie ou l'évolution des processus démocratiques.
*Pour tout résoudre cliquez ici* {% cite morozov_pour_2014 %} est publié en 2013 – 2014 pour la traduction française – et fait suite à *The Net Delusion : The Dark Side of Internet Freedom* publié en 2011.
Evgeny Morozov y prolonge sa critique d'une vision d'Internet idyllique voire fantasmée, non pas dans une démarche technophobe, mais plutôt dans une tentative d'esprit critique et philosophique visant à interroger en profondeur nos conceptions, usages et perceptions de la technologie dans un monde désormais imprégné de numérique.
Nous devons noter le caractère *grand public* de cet ouvrage, *Pour tout résoudre cliquez ici* n'est pas un travail universitaire à proprement parler, quand bien même Evgeny Morozov construit sa pensée de façon méticuleuse et référence nombre de penseurs et d'ouvrages.

Au moment de la publication de cet ouvrage[^sadin] Evgeny Morozov est clairement en opposition à une majorité – au moins médiatique – enthousiaste des technologies numériques et d'Internet : entrepreneurs, ingénieurs, intellectuels et chercheurs, souvent en lien avec la Silicon Valley.
Evgeny Morozov peut parfois être perçu comme un trouble-fête dans un univers numérique à l'apparente pensée homogène, ce livre a justement suscité un débat parmi les défenseurs du solutionnisme ou même de la part d'autres critiques des technologies {% cite wu_book_2013 %}.
Il incarne la figure d'une critique peut-être trop rare qui nous pousse à dépasser les arguments marketing de la dernière application à la mode, ou à porter un regard nouveau sur nos usages quotidiens des technologies.

Dans cet essai, Evgeny Morozov aborde le concept de "solutionnisme", qui peut être grossièrement résumé en la simplification d'un problème pour y appliquer des solutions prêtes à l'emploi, sans réelle prise en compte du contexte.
Ce solutionnisme, au contact des technologies numériques et d'Internet en première ligne, se meut en "webcentrisme", qui consiste en l'application d'Internet à n'importe quel problème ou défi majeur – de la réduction des déchets à l'amélioration de la démocratie.
Evgeny Morozov critique le solutionnisme en général et le "webcentrisme" en particulier, qui sont pour lui des conséquences directes d'un manque de réflexivité envers les technologies, de l'absence d'approche philosophique de la technique, ou d'une recherche béate de l'efficacité maximale.
Le solutionnisme conduit à une évolution de l'humain alors considéré comme une machine optimisable, tout comme son environnement ; l'autonomie d'êtres – pourtant doués d'intelligence – est tout simplement en voie de disparition.
Loin de s'opposer à la technologie, Evgeny Morozov souhaite en défendre une autre conception, et un recours qui ne devrait pas être systématique.

*Pour tout résoudre cliquez ici* est constitué de neuf chapitres, les deux premiers présentent le "solutionnisme" et le "webcentrisme", concepts qu'Evgeny Morozov critiquera ensuite dans les six chapitres suivants à travers des analyses thématiques : l'ouverture de l'administration par la publication de documents jusqu'ici peu visibles, l'évolution de la politique et de son fonctionnement, l'usage des données personnelles via des algorithmes, la gestion du crime avec les technologies numériques, et l'exploitation des données personnelles et ses conséquences avec le transhumanisme.
Le dernier chapitre est une synthèse des critiques formulées dans les parties précédentes.
Nous nous concentrerons ici sur les deux premiers et le neuvième chapitres, et nous analyserons ensuite les processus de publication avec les outils critiques mis à disposition par Evgeny Morozov.


## 1.2.1. Une critique du solutionnisme
Dans le premier chapitre de *Pour tout résoudre cliquez ici*, Evgeny Morozov présente plusieurs projets conçus pour répondre à des problèmes importants, chacun reposant sur des technologies de mesures connectées à Internet.
Dans ces exemples les solutions semblent ingénieuses, simples, et *efficaces*.
Mais, systématiquement, l'homme et son environnement sont limités à quelques données mesurables et modifiables.
Les "innovateurs" – comme les appelle Evgeny Morozov – à l'origine de poubelles connectées ou de gestion du traffic grâce aux téléphones intelligents, trouvent des solutions techniques à des problèmes complexes, mais en simplifiant ces problèmes.
Nous sommes là dans des cas de "solutionnisme", et par "solutionnisme" Evgeny Morozov entend : limiter les "situations sociales" pour pouvoir les perfectionner, bien souvent dans une démarche de recherche de résultats rapides et visibles, à court terme.
"Ce qui pose problème n'est pas les solutions qu'ils proposent, mais plutôt leur définition même de la question." (p. 18) La position des *solutionnistes* présente un autre écueil en plus de simplifier dangereusement : trouver des problèmes là où il n'y en a pas.

Pour critiquer le solutionnisme, Evgeny Morozov fait référence à plusieurs penseurs modernes ou contemporains : Ivan Illich[^ivan-illich] à propos des systèmes d'enseignement, Jane Jacobs[^jane-jacobs] à propos de l'urbanisme, Michael Oakeshott[^michael-oakeshott] à propos des rationalistes, Hans Jonas[^hans-jonas] à propos de la cybernétique, James Scott[^james-scott] concernant la démocratie, ou encore Albert Hirschman[^albert-hirschman] et son système de trois variations – effet pervers, inanité et mise en péril.
Le point commun entre ces penseurs est qu'ils considèrent tous que les solutionnistes ont une faible compréhension de leur environnement, de "la nature humaine" (p. 20).
Evgeny Morozov insiste sur le fait que sa critique du solutionnisme ne consiste pas en une peur de l'échec – une mauvaise solution à un problème pourrait avoir des conséquences néfastes –, mais en deux constats : premièrement le solutionnisme détourne le problème initial pour pouvoir trouver une solution ; deuxièmement le solutionnisme remplace la décision – ou l'indécision – par des calculs, des prévisions, des mesures, des process de surveillance.
L'humain devient alors robot asservi.
La fin de la deuxième partie de ce premier chapitre se conclut sur cet énoncé limpide : "Le rejet du solutionnisme n'est pas celui de la technologie." (p. 26)

L'objectif d'Evgeny Morozov est de démonter le solutionnisme : celui-ci a été grandement développé ces trente dernières années par "l'internet", ce concept souvent mal défini, et qui a pris plus d'ampleur encore avec le "webcentrisme" (p. 28).
Le "webcentrisme" est la tendance à placer Internet au centre de tous les discours et de tous les projets, sans forcément bien circonscrire ce concept de réseau de réseaux.
"Révéler le webcentrisme pour ce qu'il est facilitera grandement l'entreprise de discrédit du solutionnisme." (p. 28) Ainsi Evgeny Morozov introduit le deuxième chapitre de son livre.

## 1.2.2. Le "webcentrisme" est le solutionnisme contemporain
Le second chapitre de *Pour tout résoudre cliquez ici* débute avec une double critique d'Internet : Internet est *flou*, même pour les médias spécialisés dans la technique/technologie ; Internet est *sacré*, pour ceux qu'Evgeny Morozov appelle les "geeks".
Aujourd'hui, Internet intègre toute technologie et aspire n'importe quelle innovation, jusqu'à former une bouillie informe impossible à définir précisément, mais pourtant bien maîtrisée – en apparence – par certains.
Le problème est autant la définition d'Internet, que celle des usages qui y sont liés, et Evgeny Morozov de prendre l'exemple de Nicholas Carr qui est enfermé dans une vision monolithique : il n'y a pas *un* "net", Internet est plus complexe que la vision fantasmée de l'auteur de *The Shallows: What the Internet is Doing to Our Brains*[^nicholas-carr].
Evgeny Morozov parle même d'"entité mythique" (p. 33) à propos de la vision qu'ont certains d'Internet, provoquant une vision binaire, dichotomique : il n'y a pas d'*avant* et d'*après*, cette évolution est diffuse, moins brusque qu'il n'y paraît.
"Tous ces penseurs considèrent "l'internet" comme une entité à la fois unique et stable, significative et instructive, puissante et indomptable." (p. 35) Nous devons porter un regard critique sur ce que nous considérons comme acquis : par exemple l'ouverture supposée de Google ou l'invisibilité administrative présumée de Wikipédia se révèlent autre si nous questionnons mieux et si nous prenons le temps de définir, avec "humilité" (p. 41).

Le "webcentrisme" regroupe des personnes ayant un même comportement caractéristique qu'Evgeny Morozov nomme l'"époqualisme".
Ainsi, les optimistes ou les pessimistes des innovations affirment l'existence d'une révolution sans l'expliquer, la justifier ou la comparer avec d'autres faits historiques.
Ils ne convoquent pas l'histoire.
Evgeny Morozov prend plusieurs exemples passés de systèmes qui ont été repris avec Internet : du crowdsourcing pour les cartes anglaises au dix-huitième siècle à la refonte d'un logo en 1936.
Globalement ce que tente d'expliquer – avec quelques effets de répétition – Evgeny Morozov, c'est que le "webcentrisme" induit de tout vouloir expliquer au prisme d'Internet, sans esprit critique.
Alors que conserver un esprit critique ne veut pas dire tout rejeter, au contraire.
"Dans un pur esprit dialectique hégélien, le webcentrisme se maintient entre les deux pôles binaires que sont le pessimisme et l'optimisme d'Internet, faisant passer toute critique à son encontre pour une manifestation supplémentaire de ces deux extrêmes." (p. 52)
Pour poser les termes d'une révolution, il faut tenir compte de "deux critères" : il ne faut pas omettre ce qui s'est passé *avant* – au risque de défendre une pensée "anhistorique" (p. 53) –, et il faut s'appuyer sur le contexte contemporain global de cette supposée révolution – au risque de mal comprendre des phénomènes que nous pouvons pourtant observer.
Mais alors n'y a-t-il pas des changements notables avec Internet ?
Oui, mais le changement n'a pas été aussi brusque que nous pourrions le penser, il n'y pas eu de réelle rupture, comme d'ailleurs plus globalement dans l'histoire de la pensée.

Dans l'avant-dernière partie de ce deuxième chapitre, Evgeny Morozov prend l'exemple de l'imprimerie pour noter combien certains penseurs ont fait des comparaisons maladroites entre Internet et d'autres révolutions techniques et sociales.
Par exemple Clay Shirky[^clay-shirky] développe toute une argumentation autour des effets sociaux et économiques des technologies d'Internet, argumentation basée sur une théorie d'Elizabeth Eisenstein[^elizabeth-eisenstein] concernant l'imprimerie et la presse : celles-ci seraient à l'origine d'une ère totalement nouvelle, et de la même façon les bouleversements engendrés par Internet seraient sans précédent.
Mais cette théorie a elle-même des limites, Elizabeth Eisenstein a tenté de marquer plus fortement une rupture qui existait mais qui n'était pas si nette, fruit d'évolutions successives :

>Les limites de l'approche d'Eisenstein ont été soulignées par de nombreux universitaires et demeurent très pertinentes dans le débat actuel sur internet. Le premier à tirer le signal d'alarme, en 1980 (soit un an après la publication du livre) fut l'intellectuel et historien Anthony Grafton. Il vilipenda Eisenstein pour avoir "tirer de ses sources les faits et déclarations semblant répondre à ses besoins polémiques immédiats". {% cite morozov_pour_2014 --locator 61 %}

La conclusion de ce second chapitre consacré au "webcentrisme" est la suivante : le webcentrisme est l'expression contemporaine et l'outil du solutionnisme, il entraîne une absence de regard critique sur les technologies.
Pire, il alimente une recherche aveugle d'efficacité, et déclenche des expérimentations basées sur celle-ci.
Si le "webcentrisme" a d'une certaine façon aidé des combats militants, il a du même coup entraîné une perte de "clarté d'analyse" et de débat.
Pour Evgeny Morozov le "webcentrisme" s'approche d'une religion, pour le critiquer il faut revenir aux bases, oublier ce que nous savons : les chapitres suivants sont consacrés à cette entreprise.
"Le fait que ce webcentrisme nous coupe de la réalité est un motif d'inquiétude, non de réjouissances." (p. 69)

## 1.2.3. Une critique en pratique
Le neuvième chapitre de *Pour tout résoudre cliquez ici* est un condensé des six chapitres précédents, ceux-ci présentant des analyses de cas de solutionnisme et de webcentrisme.
Evgeny Morozov commence ce dernier chapitre en présentant le projet d'un parcmètre "intelligent" qui permet de mesurer l'usage des places de parking à Santa Monica, et d'améliorer le système de stationnement.
Evgeny Morozov y applique les principes d'Albert Hirschman : inanité, effet pervers et mise en péril.
Ce projet présente ces trois risques, puisque l'objectif final n'est pas l'amélioration de la ville en cela qu'elle est l'amélioration des citoyens et de leur situation – en tant que ceux-ci auraient la possibilité d'exprimer un choix, de décider – mais cet objectif est uniquement celui du profit financier de la Ville.
Il ne suffit pas de relever les points facilement résolubles d'un problème, mais il faut considérer le problème dans toute sa complexité, et chercher à redéfinir les causes et les effets "en terme de relations, structures, et processus." "Le vrai problème avec le système de Santa Monica n'est pas qu'il soit intelligent, mais qu'il ne le soit pas suffisamment." (p. 314)

Là où nous nous trompons, c'est sur le fait de vouloir distinguer les solutions techniques d'un côté et les questions éthiques, sociales ou morales de l'autre.
Les deux sont forcément imbriquées.
Et Evgeny Morozov d'évoquer Jacques Ellul comme grand défenseur de cette séparation de la "moralité" et de la "technologie" : "Il est temps d'abandonner ce discours sur la "Technologie" avec un _T_ majuscule pour nous consacrer plutôt à découvrir de quelle manière d'autres technologies peuvent favoriser ou nuire à la condition humaine." (p. 315) Là où Jacques Ellul considère l'homme comme soumis à une technique désormais hors de contrôle, celle-ci ayant dépassé le simple statut de moyen ou d'outil, Evgeny Morozov soutient qu'il faut dépasser ce constat et s'organiser pour chercher et trouver les meilleures solutions, sans vouloir rejeter la technologie par principe.
Il faut prendre en compte que les humains que nous sommes et le monde que nous habitons ne sont pas optimisables comme des machines : le niveau de complexité des solutions est proportionnel au contexte des problèmes auxquelles elles répondent.
La technologie peut aussi proposer un choix à l'humain qui l'utilise, et donc déplacer la solution : non plus dans un fonctionnement technique binaire – appuyer sur un bouton – mais comme processus de décision que va prendre un être conscient et réfléchissant.
Evgeny Morozov présente plusieurs exemples qui vont dans ce sens, notamment des "appareils erratiques" ou des "produits transformationnels" (pp. 317 et 319).

Evgeny Morozov aborde le fonctionnalisme – probablement pour dépasser le solutionnisme – et le définit ainsi : les objets technologiques ont des fonctions et des objectifs, il s'agit alors de chercher à concevoir et produire des objets qui expriment cela le plus fortement possible.
Le fonctionnalisme pousse les concepteurs, les designers, à ne pas penser des objets "initiateurs de débats" (p. 320).
Pour Evgeny Morozov il faut modifier les objectifs du fonctionnalisme – et donc indirectement ceux du solutionnisme : non plus chercher le meilleur et le plus efficace des objets technologiques pour effectuer telle tâche, mais permettre une relation entre la machine et son utilisateur qui donne lieu à une solution.
Et c'est là le rôle du designer que d'ouvrir un dialogue entre l'objet et son utilisateur via l'interrogation de l'origine de cet objet et des usages causés par ce dernier.
Le design, par le biais de ces "artefacts", doit susciter une discussion qui mènera aux solutions via des usages non prévus ou insoupçonnés.
Pour prolonger sa réflexion Evgeny Morozov prend un détour avec des exemples dans le domaine de l'économie d'énergie.
À travers ceux-ci, il nous incite à passer d'une "conscience pratique" à une "conscience discursive" (p. 324) : la conscience pratique correspond à une mesure de nos consommations, à un rapport assez rationnel avec la technologie, alors que la conscience discursive correspond à une "introspection" à la fois personnelle et collective.

Internet, en tant qu'il est désormais le vecteur le plus important de l'information, n'est-il pas trop fonctionnaliste, et notamment à travers les services et outils qui nous permettent de l'utiliser ?
Evgeny Morozov compare donc le fait de laisser un objet électrique en veille et le fait de consommer de l'information à outrance via de nombreux agrégateurs et fermes de contenus.
Il est possible de rendre visible notre consommation d'information et son impact, alors pourquoi ne pas le faire ?
Moralité et prise de décision ne sont pas des _denrées_ limitées comme le pétrole, comme veulent nous le faire croire nombre de penseurs solutionnistes, mais plutôt des éléments qui s'auto-génèrent à mesure que nous en créons.
Il ne faut plus faire appel à la psychologie à travers le solutionnisme, mais plutôt à la philosophie.
Ainsi la question du choix est centrale : nous pouvons avoir l'impression de choisir – par exemple entre un produit de grande distribution et un produit bio –, alors que notre choix n'aura que peu d'incidence sur un plan global, et encore moins sur nous puisqu'il n'y aura plus de prise de conscience accompagnant ce choix.

La sérénité, l'absence de conflit et l'absence de faille sont une influence de ceux qu'appellent Evgeny Morozov les "geeks" : la technique pouvant être optimisée continuellement, pourquoi ne pas faire la même chose avec l'humain ?
L'exemple de la vie privée permet de comprendre que la contrainte peut être une bonne chose : "Le but de la vie privée n'est pas de protéger un soi stable de l'usure, mais de créer des frontières entre lesquelles il puisse jaillir, évoluer et se stabiliser.
En d'autres termes, les limites et les contraintes peuvent être productives, bien que toute l'arrogance de "l'internet" suggère le contraire." (p. 337)
Au risque de nous répéter, la perte d'autonomie ne semble pas problématique lorsque c'est pour une bonne cause – Evgeny Morozov prend l'exemple de l'économie d'énergie –, mais il y a pourtant un double "écœurement" : la "ruse" utilisée pour nous convaincre d'agir ainsi, et le fait que nous ayons l'impression d'avoir une autonomie dans ce choix.
"Tenter d'améliorer la condition humaine en supposant tout d'abord que les personnes sont comme des robots ne nous mènera pas bien loin." (p. 340)

La conclusion de ce chapitre, qui est également la conclusion de *Pour tout résoudre cliquez ici*, prend la forme d'un avertissement et d'une ouverture : si le principal défaut des ingénieurs est de reconsidérer perpétuellement les situations – "Pour eux, tout est négociable, y compris la dignité et l'autonomie." (p. 342) –, alors il faut réintroduire de l’irrationalité, des débats, des délibérations, des questionnements dans la façon dont nous concevons et nous utilisons la technologie.
Le solutionnisme doit intégrer du doute pour dépasser ses propres limites.

## 1.2.4. Esprit critique et question du choix
*Pour tout résoudre cliquez ici* s'inscrit dans un mouvement de pensée minoritaire lorsqu'il est publié en 2013, Evgeny Morozov se positionnant résolument contre une partie des innovateurs et des essayistes des nouvelles technologies, et cela sur le même terrain : *Pour tout résoudre* est en effet un livre destiné au *grand public*, nous n'avons pas affaire à un travail purement universitaire.
Comme il le démontre lui-même il n'est pas seul à remettre en cause une certaine approche de la technique et de la technologie, ses références sont autant des chercheurs contemporains que des intellectuels des vingtième et dix-neuvième siècles.
Sa proximité avec ces courants est principalement philosophique, en cela que la philosophie cherche à définir précisément une question, avant même de répondre à cette question.
Derrière des propos qui semblent souvent pessimistes, négatifs voir alarmistes, Evgeny Morozov construit une vision de la technique proche de celle du philosophe Gilbert Simondon : la technique n'est pas mauvaise en soi, mais c'est notre rapport avec elle qu'il faut reconsidérer.
Le constat froid et alarmiste d'une technique qui assujettit l'homme doit être dépassé par une nouvelle considération de la technique et de son application : nous devons nous repositionner par rapport à la technologie.
Le penseur d'origine biélorusse va plus loin : les designers et les ingénieurs ont le choix de concevoir des objets, des artefacts ou des systèmes qui ne considèrent pas l'humain comme un robot, de laisser de côté le solutionnisme et le "webcentrisme" pour se concentrer sur des approches _techniques_ véritablement humaines.

## 1.2.5. Des processus de publication solutionnistes ?
Nous allons projeter la critique du solutionnisme d'Evgeny Morozov sur un objet qui, contrairement au domaine choisi dans *Pour tout résoudre cliquez ici*, n'est pas directement social : prenons les processus et les outils de publication.
Actuellement, les logiciels les plus utilisés par les maisons d'édition sont les traitements de texte et les logiciels de publication assistée par ordinateur.
Quels sont les objectifs de ces outils ?
Faciliter le travail de publication, qui consiste en plusieurs tâches : la structuration de contenus, les interactions sur un texte, la mise en forme, la génération des formats permettant une diffusion papier ou numérique, et l'archivage des fichiers de travail.
Les logiciels en question sont conçus pour être relativement simples d'utilisation, masquant des aspects pourtant importants : distinction du fond et de la forme, compréhension des enjeux liés à certains formats, qualité du code dans le cas du livre numérique, etc.
L'utilisation d'un logiciel de publication assistée par ordinateur ou d'InDesign – pour le citer –, se fait donc presque *à l'aveugle*, si nous prenons en compte qu'il s'agit uniquement d'une relation entre une interface et un utilisateur, et que le fonctionnement de cette interface reste opaque.
Remettre en cause ces processus ne consisterait pas à complexifier des tâches qui n'auraient pas besoin de l'être, mais à replacer de la transparence dans des procédés nécessitant une compréhension et une réflexivité.

Si les besoins d'une maison d'édition peuvent différer selon sa spécialité, les outils restent globalement les mêmes – ce qui peut paraître étonnant en comparaison d'autres métiers –, et par ailleurs les évolutions des logiciels en question sont unilatérales : il n'y a pas de retour en arrière possible lors d'une mise à jour par exemple.
Un même outil cherche à résoudre des problèmes très différents et tente de s'adapter à tout type de situation : il finit par faire mal les choses, et à imposer de mauvais mécanismes.
Nous retrouvons les traits du fonctionnalisme, et dans certains cas d'utilisation il s'agit même de solutionnisme : la recherche d'efficacité masque les objectifs initiaux de tels outils.
Les solutions mises en place visent à résoudre les problèmes les plus évidents, simplifiant et éludant jusque l'une des composantes essentielle de l'édition : la différenciation entre la structure – le texte lui-même et les valeurs qui lui sont attribuées – et la mise en forme – les caractéristiques graphiques attribuées à la structure.
La marge de décision de l'utilisateur est presque inexistante.
Il faut pourtant que l'objet technique provoque une réflexion chez l'utilisateur, et ainsi que ce dernier résolve un problème.
C'est bien dans cette relation avec l'utilisateur qu'un problème peut être résolu, et non en appuyant sur un bouton.
Les traitements de texte et les logiciels de publication assistée par ordinateur sont clairement du côté du choix rationnel : la première contrainte est de résoudre un problème – concevoir et produire un livre, donc *publier* –, quand bien même ce problème n'est pas défini – en quoi consiste exactement *publier* ?
Le recours à un logiciel monolithique comme InDesign oblige à enfermer sa vision dans un processus et pas dans un autre.

L'automatisation de certaines étapes d'une chaîne de publication est nécessaire, des outils adaptés sont donc incontournables pour un éditeur.
Mais, en plaçant la convivialité ou la modularité comme principe de départ, d'autres modes de fonctionnement sont possibles, qui ne visent pas l'efficacité directe de l'outil mais la maîtrise de l'*activité* de publication.
La constitution du collectif PrePostPrint {% cite fauchie_workshop_2017 %} est une initiative qui va dans ce sens, l'exemple le plus marquant porté par cette démarche de recherche et de démocratisation est l'utilisation des langages HTML et CSS pour fabriquer et produire des publications imprimées, aussi appelée HTML2Print.
HTML et CSS sont des standards : l'un est conçu pour structurer des contenus, et le second pour déterminer une mise en forme à partir de la structure.
Le recours aux technologies du web dans ces cas n'est pas une application du "webcentrisme" à la publication, mais plutôt l'utilisation réfléchie et adéquate d'une technologie dans une situation et une période données.
Par ailleurs ce type de procédé implique un travail de conception, d'apprentissage et de remise en cause régulière, donc de l'humain là où les chaînes de publication sont actuellement surtout composées de logiciels.

Par ailleurs, au-delà du choix technologique – logiciel ou langages HTML et CSS – il faut mentionner celui de la forme des collaborations.
PrePostPrint, ou des initiatives préexistantes comme les workshops d'Open Source Publishing[^osp] ou d'autres structures, sont autant des temps destinés à la conception et à la production, et basés sur des procédés originaux, que des occasions qui portent des discussions et des échanges autour de problèmes et de solutions.
L'objectif n'est pas uniquement de produire, mais de permettre un débat, débat qui peut par ailleurs se prolonger sous d'autres formes – via des sites web, du partage de code, etc.

Evgeny Morozov nous donne des clés pour construire une critique d'un solutionnisme présent dans notre rapport aux technologies, y compris dans le domaine de l'édition.
Remettre en cause des processus opaques n'est pas une position négative qui rejette toute solution technique, mais la possibilité de ré-introduire de l'humain et de rechercher la meilleure solution à un problème en fonction du contexte.
*Meilleur* au sens où la maîtrise et la liberté de choix doivent être conservés, où l'humain doit être au centre des procédés, et où la remise en cause doit être permanente, si ce n'est perpétuelle.
La pensée de Gilbert Simondon, comme nous l'avons déjà évoquée, précède et prolonge ces réflexions, nous l'analysons dans la prochaine partie.

[^silicon-circus]: C'est ainsi qu'est présenté le blog Silicon Circus auquel participe grandement Evgeny Morozov, blog hébergé par *Le Monde diplomatique* [http://blog.mondediplo.net/-Silicon-circus-](http://blog.mondediplo.net/-Silicon-circus-).
[^sadin]: Il faut noter combien les critiques ou les *pessimistes* du numérique sont plus nombreux depuis plusieurs années : le très médiatique écrivain et philosophe Éric Sadin et sa critique de la "Siliconisation du monde" et du "technopouvoir", les activités des Éditions L'Échappée, ou encore certains écrits du philosophe Bernard Stiegler.
[^ivan-illich]: Ivan Illich est un penseur de l'écologie politique du vingtième siècle. Parmi ses nombreux travaux et ouvrages, il a notamment mis en place une critique de la société industrielle et du fonctionnement de ses structures (école, médecine, transports).
[^jane-jacobs]: Jane Jacobs est une auteure et philosophe américaine de l'urbanisme et de l'architecture. Ses études et théories, basées sur l'observation, ont eu une influence importante sur l'urbanisme nord-américain.
[^michael-oakeshott]: Michael Oakeshott est un historien et philosophe anglais dont les travaux se sont principalement concentrés sur la pensée politique, l'éducation, la philosophie de l'histoire ou encore la religion.
[^hans-jonas]: Hans Jonas est un philosophe allemand. *Le Principe responsabilité* est son ouvrage le plus connu, il aborde la question de la technique en relation avec l'existence de l'humanité.
[^james-scott]: James Scott est un chercheur américain dans le domaine des sciences politiques.
[^albert-hirschman]: Albert Hirschman est un économiste américain dont les recherches ont porté sur l'économie du développement ou l'économie politique.
[^nicholas-carr]: Nicholas Carr est un auteur américain connu entre autres pour son article "Is Google Making Us Stupid?" publié en 2008 dans The Atlantic, portant sur les effets néfastes que pourrait avoir Internet sur notre cerveau.
[^clay-shirky]: Clay Shirky est un journaliste américain spécialisé dans les nouvelles technologies de l'information et de la communication. Il est l'auteur de *Here Comes Everybody: The Power of Organizing Without Organizations*, publié en 2008, qui compare le phénomène lié aux plates-formes de blog et aux réseaux sociaux avec l'apparition de l'imprimerie.
[^elizabeth-eisenstein]: Elizabeth Eisenstein est une historienne américaine dont les travaux les plus connus concernent l'histoire de l'imprimerie.
[^osp]: Open Source Publishing, ou OSP, est un collectif d'individus dont les intérêts et les compétences vont du design graphique à la typographie en passant par le développement, la performance ou la cartographie. OSP a organisé un nombre important de workshops, notamment avec des étudiants, autour de la question de la publication. Voir en ligne : [http://osp.kitchen/about](http://osp.kitchen/about)
