---
title: "2.3. Distill"
category: 2. Expérimentations
order: 4
partiep_link: /2-experimentations/2-2-getty-publications/
parties_link: /3-un-systeme-modulaire/3-0-introduction/
repo: _memoire/2-experimentations/-2-3-distill.md
---
{% include_relative -2-3-distill.md %}
