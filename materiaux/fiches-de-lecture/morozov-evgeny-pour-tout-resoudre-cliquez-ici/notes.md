# Notes sur : Morozov, Evgeny, Pour tout résoudre cliquez ici

## Notice bibliographique de l'ouvrage étudié

## Notes diverses

### Introduction
[Page 7] Evgeny Morozov prend la Silicon Valley comme terrain d'observation, en tant qu'elle est le lieu où les entrepreneurs souhaitent changer le monde, en tentant "la résolution de problèmes générés par d'autres personnes". Les deux premiers exemples sont les PDG de Google et de Facebook, qui ne parlent pas de faire des bénéfices financiers, mais de "rendre le monde meilleur" pour reprendre les mots d'Eric Schmidt. La Silicon Valley est emblématique de cette vision, mais elle n'est pas la seule.

[Page 8] La quête de la Silicon Valley n'est pas tant de résoudre tel ou tel problème, il s'agit plutôt d'une démarche globale, d'une attitude : "parvenir à une efficacité optimale". Une attitude qui prend beaucoup de place, qui peut toucher tous les domaines, et qui convainc les financeurs, mais qui devient une "orgie du perfectionnement" selon Evgeny Morozov.

[Page 8-9] Evgeny Morozov décrit en creux la vision de l'humain qu'ont les spécialistes de la Silicon Valley : un homme machine, qui n'est en fait qu'un système qui peut toujours être perfectionné – sommeil, alimentation, mémoire, etc. –, et dont chaque problème a forcément une solution – solution technique sous forme de produit. L'homme en tant qu'être vivant, mais également en tant qu'individu ayant des activités : son temps peut également être perfectionné. Et ce n'est pas que l'homme lui-même, il s'agit également d'un ensemble d'hommes, d'une société, qui peut être constamment améliorée, y compris sa démocratie – ou ce qu'il en reste.

[Page 9-10] Ce que décrit Evgeny Morozov, c'est un monde où tout est bien réglé, où l'erreur n'a plus sa place, où l'environnement doit être parfait, et si ce n'est pas le cas, il y aura toujours une solution pour que ça le soit. "Mais tout n'aura pas changé : le système aura toujours besoin d'humains imparfaits pour générer les clics qui pomperont l'argent des publicitaires."

[Page 10] Les exemples pris par Evgeny Morozov ne forment pas une "dystopie", ce n'est pas une fiction ou une utopie qui virerait au cauchemar : c'est une réalité proche, atteignable. {Je ne comprends pas l'utilisation de dystopie par Evgeny Morozov, dans le sens où pour moi cela peut être une utopie qui vire au cauchemar. Ce que veut dire Evgeny Morozov c'est que c'est peut-être dès l'origine un cauchemar ? Qu'il n'y a pas d'utopie dans ce projet ?}

[Page 10-11] Si Evgeny Morozov trouve ce "bref aperçu" "terrifiant" : ce n'est pas parce qu'il pourrait être critiqué dans sa faisabilité, au contraire il faut prendre au sérieux les "innovateurs de la Silicon Valley", tout cela est faisable, est techniquement réalisable.  
"Ainsi donc, peut-être devrions-nous envisager la possibilité que la Silicon Valley ait effectivement les moyens de réaliser une partie de ses projets les plus fous."

[Page 11] Il ne faut pas se concentrer sur une critique de la faisabilité, il faut interroger la raison de ces entreprises – entreprises au sens d'entreprendre –, "la pertinence des objectifs", et les moyens mis en place. Dans cet ordre – fins puis moyens – et non l'inverse. "J'interroge à la fois les fins et les moyens de la Silicon Valley sur ses dernières tentatives pour 'résoudre les problèmes'."  
Le simple fait de croire à cette amélioration sans limite pose la question de notre regard critique sur cette entreprise, sur cette démarche. Mais puisque ces projets se veulent fondamentalement humanistes, comment les critiquer ? Comment faire accepter une critique de ces démarches qui visent à sauver le monde ?

[Page 11-12] Le "postulat" de *Pour tout résoudre cliquez ici* est le suivant : en vantant les mérites de l'"efficacité, de la transparence, et de la certitude et de la perfection", et en supprimant "les tensions, l'opacité, l'ambiguïté et l'imperfection", la Silicon Valley limite notre capacité réflexive, et souhaite nous "enfermer" dans une prison numérique.  
L'objectif du livre est de définir le "coût réel" de l'utopie, du "paradis" que dessine la Silicon Valley, et d'expliquer pourquoi nous ne l'avons pas défini plus tôt.

[Page 12] Evgeny Morozov pose la question de la liberté : est-ce qu'une quête de perfection n'est pas aussi la fin de la possibilité et de l'expression de nos libertés, tant du point de vue individuel que collectif ?

[Page 12] Le plan du livre est composé en deux temps : aperçu et critique de deux idéologies dominantes, puis application de ces idéologies dans différents cadres. En détail :

1. aperçu et critique du "solutionnisme"
2. aperçu et critique du "webcentrisme"
3. "promouvoir la transparence"
4. "modifier le système politique"
5. "accroître l'efficacité dans le secteur culturel"
6. "réduire le crime" à partir des données et de leur traitement
7. "quantifier le monde"
8. ludification dans le domaine civique
9. dépassement du solutionnisme et du webcentrisme, comment la technologie peut être conçue puis utilisée "dans le but de répondre aux besoins citoyens et humains"

[Page 12-13] Mais pourquoi cette critique ? Pourquoi Evgeny Morozov s'oppose à cette quête de perfection ? Alors même que cela est *faisable* aujourd'hui ? Pour deux raisons principales : le fait que l'on impose quoi réparer, ou quoi améliorer ; et l'imperfection est parfois plutôt une spécificité, un "trait caractéristique".

[Page 13] Objectif final de ce livre : savoir repérer la "mentalité solutionniste", savoir la combattre.  
{N'est-ce pas là un brin moralisateur ? Evgeny Morozov ne se place-t-il pas lui-même comme sauveur du monde ? Et comme donneur de leçon ?}

### Chapitre 1 : Le solutionnisme et ses désillusions
[Page 14-15] Evgeny Morozov prend l'exemple des déchets comme degré ultime de mesure et de partage des données privées. L'objectif de BinCam est de partager l'image de sa poubelle, ou plutôt ce qu'elle contient, via un jeu, pour responsabiliser les utilisateurs. Cette idée, peut-être bienveillant, pose beaucoup de questions.

[Page 15] Dans le cas de BinCam, Evgeny Morozov pose une question simple : est-ce que le fonctionnement pensé par ces entrepreneurs n'est-il pas trop simpliste ? Est-ce que l'homme et son environnement peuvent être limités à quelques données ? Les choses sont beaucoup plus complexes qu'il n'y paraît, et quand bien même cela fonctionnerait, ce ne serait probablement qu'à un niveau individuel, et non à un niveau collectif.

[Page 16] L'exemple de BinCam révèle d'autres choses : les moyens disponibles sont récents et évolueront encore – de la poubelle connectée au traitement des données ; l'intimité était plus forte ; une "standardisation" s'est opérée : notamment via une plateforme comme Facebook.

[Page 17] Deux innovations récentes notables sont à la base de projet de perfection du monde : "technologie à base de capteurs" et la connexion entre individus par les réseaux sociaux.

[Page 17] {La question n'est pas de savoir quelle fin est visée, ou quels moyens sont mis en place pour résoudre un problème, mais l'association des deux, pourquoi telle fin avec tel moyen ? "La pertinence de l'objectif".}

[Page 17] Puisqu'un smartphone enregistre nos données, et que ces données peuvent être exploitables, le simple fait de le porter avec soit pourrait être un acte citoyen. Exemple du traffic routier : en sachant où l'on est et à quelle vitesse nous allons, Google peut définir le traffic.

#### Un désir d'amélioration (dans tous les domaines !)
[Page 18] Evgeny Morozov définit le terme de "solutionnisme" : chercher à perfectionner une activité bien définie, bien souvent à court terme. Les "situations sociales" se voient tout simplement limitées en cela qu'elles peuvent être perfectionnées, ou les problèmes qu'elles posent peuvent être résolus. Si l'on prend du recul, cette démarche peut causer plus de tord que de bien.  
Bien souvent, les problèmes sont plus complexes que la façon dont ils sont exposés : "Ce qui pose problème n'est pas les solutions qu'ils proposent, mais plutôt leur définition même de la question." Une démarche solutionniste cherche à répondre à un problème avant de l'avoir bien défini.  
{Il s'agit ici d'un problème philosophique, en cela que la philosophie cherche à définir précisèment une question, avant même de répondre à cette question.}  
"Savoir de quoi se composent les problèmes importe tout autant que de savoir comment les résoudre."

[Page 18-19] L'autre problème, déjà souligné plus haut (pp. 12-13), est que là où les solutionnistes voient des problèmes, il n'y en a pas forcément, "derrière ces vices se dissimulent souvent des vertus."

[Page 19] Evgeny Morozov fait référence à Albert Hirschman, car il va utiliser les trois variations établies par Hirschman – effet pervers, inanité, mise en péril – pour critiquer le solutionnisme, et non le conservatisme ou la résistance conservatrice. Evgeny Morozov ne va pas vers l'"inaction", mais l'"urgence du problème" n'oblige pas à appliquer la dernière technologie à la mode. Il est nécessaire d'avoir une réfléxivité sur le problème même.

[Page 19-20] Evgeny Morozov fait appel à d'autres penseurs pour signaler que sa remise en question du solutionnisme peut se rapprocher d'autres courants : Ivan Illitch à propos des systèmes d'enseignement, Jane Jacobs à propos de l'urbanisme, Michael Oakeshott à propos des rationalistes, Hans Jonas à propos de la cybernétique, James Scott concernant la démocratie, Friedrich Hayek à propos des "planificateurs centraux". {Nous pourrions ajouter Gilbert Simondon à propos de la cybernétique ou de l'automatisme, voir même de la technique.}

{La pensée de Evgeny Morozov pourrait être synthétisée par une invitation à la critique, à poser les détails d'un problème, à réfléchir avant d'agir.}

[Page 20] Le point commun entre ces penseurs est qu'ils considèrent tous que les solutionnistes ont une faible compréhension de leur environnement, de "la nature humaine".

{Dessinons une lecture du solutionnisme appliqué aux chaînes de publication : dans le cas d'InDesign, un même outil chercher à résoudre des problèmes très différents, en tentant de s'adapter à tout type de situation, il finit par faire mal les choses, et à imposer des mauvais mécanismes (conception de livre, habitudes, confusion entre forme et fond). L'abstraction est trop forte. Il faudrait des exemples ici. Nous retombons sur des concepts de Gilbert Simondon, un objet technique abstrait serait en fait un objet technique pensé par un solutionniste !}

[Page 20] Une solution à un problème précis ne pourra être que rarement appliquée à un autre problème, ou au même problème dans un autre contexte.

[Page 20-21] Evgeny Morozov l'a déjà dit plus haut, le problème n'est pas le risque d'échec, mais le fait que la question initiale ait été modifiée pour que la solution puisse s'y appliquée.  
"Le problème n'est pas que les solutions proposées risquent d'échouer, mais plutôt que les solutionnistes, en voulant résoudre le problème, le tordent d'une façon si lamentable et inhabituelle que lorsqu'ils en viennent à bout, il est devenu une tout autre chose."

[Page 21] Exemple de l'enseignement : les cours en ligne ne peuvent résoudre tous les problèmes de l'enseignement, ou alors cela peut être le cas si on limite l'enseignement à une connaissance brute, ce qui n'est pas le cas, l'enseignement doit permettre un regard critique, et les cours en ligne ne peuvent être faits pour apprendre cela.

[Page 22] Le "danger" du solutionnisme est de proposer des solutions rapides, à court terme, mais qui auront des répercussions importantes sur le long terme, des "conséquences politiques" : notamment le fait de casser une refonte plus importante, mais plus longue, et plus "exigeante". {Est-ce qu'une démarche d'amélioration progressive et continue ne pourra pas résoudre cette difficulté ? Modifier le système actuel sans le supprimer, par itération (si ce n'est pas pertinent on revient en arrière), et en continu dans le temps (et non pas en cassant tout et en proposant tout de suite une solution à la va-vite). Deuxième critique : est-ce que parfois des solutions rapidement mises en place ne peuvent pas représenter un vrai bénéfice, je pense à l'expérience de Joli code qui a mis en place en deux jours une plateforme parce que très peu de moyens, en simplifiant le processus d'interaction avec la plateforme.}

[page 22] Evgeny Morozov prend l'exemple de la cuisine avec Michael Oakeshott : le problème des concepteurs de livres de cuisine, c'est qu'ils considèrent que le livre seul permet de cuisiner, alors qu'il y a beaucoup de connaissances implicites nécessaires, en plus du livre. "Quelles que soient les règles, recettes et formules contenues dans le livre, elles ne prennent sens qu'à travers leur interprétation et application au sein de la pratique culinaire."

[Page 23-24] Le problème induit par la technologie dans le cas de la cuisine est qu'elle permet d'aller plus loin que le livre de cuisine, mais elle remplace les pratiques – et le plaisir qui y est lié –, par des opérations dictées. Il n'y a plus de place pour la décision, ou justement l'indécision, tout est calculé, prévu, mesuré, surveillé, "optimisé". L'humain devient robot asservi.

[Page 24] Les systèmes vendus comme étant augmentés sont plutôt diminuant sur un plan intellectuel : il n'y a plus de place pour l'intellect, l'augmentation va dans le sens où justement il n'y a plus d'incertitude, plus d'effort... Ceux qui pensent et créent cette augmentation ne prennent pas en compte les possibilités offertes par les "défis" et les "obstacles". La dimension de "stimulation" est oubliée, au profit d'une efficacité qui annihile.

[Page 25] Commencer à mesurer une activité engendrera la mesure de toutes nos activités, voir une surveillance : c'est un "cheval de Troie".  
Pourtant, nous dit Evgeny Morozov, la technologie peut permettre une création, accroître notre plaisir et nous stimuler.  
"Célébrer l'innovation pour elle-même est de mauvais goût."

[Page 26] "Le rejet du solutionnisme n'est pas celui de la technologie." L'humain et son environnement ne doivent pas être simplifiés, il faut voir la complexité pour mieux y répondre.

#### Pasteur et Zynga
[Page 26] "Internet" n'est pas compris comme ce qu'il est – un réseau de réseaux techniques –, mais plutôt comme un écosystème d'innovations. Il y a déjà à ce niveau un problème de définition.

[Page 27] Evgeny Morozov cherche à comprendre pourquoi Internet est la source du solutionnisme contemporain. "En d'autres mots je cherche à comprendre pourquoi et comment 'l'internet' stimule, mais aussi pour quelle raison et de quelle manière il peut dérouter."

[Page 27-28] Evgeny Morozov fait le constat qu'internet a permis d'alimenter et d'offrir un terrain plus vaste aux "attitudes solutionnistes", développant même ce qu'il appelle le "webcentrisme" : "Mais il [internet] leur a également procuré un ensemble d'hypothèses sur la façon dont le monde fonctionne et devrait fonctionner, s'exprime et devrait s'exprimer, transformant tous les sujets et débats d'une manière incontestablement webcentrique."

[Page 28] L'objectif d'Evgeny Morozov est de démonter le solutionnisme : celui-ci a été grandement développé par "l'internet", ce concept souvent mal défini, et qui a pris plus d'ampleur encore avec le "webcentrisme". "Révéler le webcentrisme pour ce qu'il est facilitera grandement l'entreprise de discrédit du solutionnisme."  
C'est l'articulation vers le chapitre 2.


### Chapitre 2 : L'absurdité de "l'internet" et comment y mettre fin
[Page 29] Le terme "internet" est : flou, même pour les médias spécialisés dans la technique/technologie ; sacré, pour ceux qu'Evgeny Morozov appelle les "geeks".

[Page 30] Aujourd'hui toute technologie est intégrée à internet, internet aspire n'importe quelle innovation, jusqu'à former une bouillie informe impossible à définir précisèment, mais pourtant bien maîtrisée par certains.  
"Cependant il y a quelque chose d'étrange dans la façon dont les geeks parviennent à clamer qu'internet est stable et durable, tout en travaillant d'arrache-pied à ce qu'il le reste. Leur théorie est diamétralement opposée à leurs actes, une discordance moderne habituelle sur laquelle ils préfèrent ne pas s'étendre."

[Page 30-31] La critique de Nicholas Carr à propos d'internet – internet provoquerait une faim insatiable de l'homme, modifiant son cerveau et ses capacités – est fondée sur une conceptualisation d'internet qui est probablement erronée : il n'y a pas *un* "net", internet est plus complexe que sa vision fantasmée. "En réalité, tant que nous persisterons à penser qu'il y a bien un 'net', ça n'ira pas mieux."

[Page 31-32] Le problème est autant la définition d'internet, que celle des usages qui y sont liés. Nicholas Carr est enfermé dans une vision monolithique d'internet, "Sa notion d'un 'net' rigide et permanent l'empêche d'identifier les réformes structurelles qui pourraient conduire à moins de destruction."

{Dans le cas d'une chaîne de publication, le recours à un logiciel monolithique comme InDesign oblige à enfermer sa vision dans un processus et pas dans un autre. Tout peut être modifié, encore faut-il bien définir le contexte, etc.}

#### Contre le courant d'internet
[Page 32-33] Evgeny Morozov parle même d'"entité mythique" à propos de la vision qu'ont certains d'internet.

{Exemple d'Offlinefirst qui est justement une critique d'internet et une recherche de solutions, en prenant en compte le caractère complexe d'internet et les usages tout aussi complexes. Les "Offline First Camp" ne sont pas un simple retrait, mais une recherche complexe, etc.}

[Page 33] Evgeny Morozov critique bien une certaine posture, qui à tendance à passer à autre chose de plus neuf et plus brillant, et oublie l'évolution qui a précédé cet *autre chose*, et qui n'en est pourtant pas déconnectée. "Dorénavant, il semble que globalement l'humanité ait opté pour leur successeurs plus jeunes, plus légers et plus efficaces."  
Il est étonnant qu'internet ne soit pas remis en cause dans son existence même, {alors qu'elle est très fragile}.

[Page 33-34] Evgeny Morozov critique une vision binaire, dichotomique : il n'y a pas d'avant internet puis internet, cette évolution est diffuse, moins brusque qu'il n'y paraît. "Il n'est pas tombé du ciel !"

[Page 34] Mais les civilisations d'époques précédentes ont-elles été capables d'envisager leur futur, comme nous devrions imaginer le nôtre sans internet ? Oui, via des fictions, et pourquoi nous ne parvenons pas à le faire ? Cela n'est pas si simple dans le cas d'internet, puisqu'il sous-tend beaucoup de technologies.

[Page 35] Pourquoi est-ce que ce serait à nous – individu et collectif – de nous adapter à internet comme le suggère plusieurs *innovateurs* ? "Tous ces penseurs considèrent "l'internet" comme une entité à la fois unique et stable, significative et instructive, puissante et indomptable."  
"On pense donc que "l'internet" possède une essence profonde, une logique propre, une téléologie, et que cette nature se déploie rapidement sous nos yeux."

[Page 35-36] Selon certains intellectuels, internet serait aussi déterminé que "des lois naturelles", comment en sommes-nous arrivés là ?  
{Evgeny Morozov fait parfois les mêmes types de raccourci que ceux qu'il critique, par exemple lorsqu'il dit que "les débats sur 'l'internet' sont devenus stériles", en fait ce ne sont pas tous les débats, internet n'est qu'un médium ou média, qui peut certes avoir une majorité d'avis convergents, mais ce n'est pas un bloc monolithique comme il le dit lui-même.}

#### La fausse pédagogie de "l'internet"
[Page 36-37] De nombreux intellectuels pensent que nous devrions nous inspirer d'internet, et qu'internet serait un épisode ou saut historique aussi fort que des événements *réellement* historiques. Evgeny Morozov prend l'exemple de Steve Johnson.

[Page 37] Aspect important d'internet : la dimension *décentralisée*.

[Page 38] Evgeny Morozov prend l'exemple de Kickstarter, avec lequel Steve Johnson le compare avec un subventionnement public : "Des sites comme Kickstarter ont tendance à favoriser les projets populistes." Les projets soutenus sont-ils de même nature ? Est-ce que les domaines concernés vont globalement gagner en qualité ?

{Evgeny Morozov met en tension d'un côté l'efficacité, la perfection, la rapidité, et de l'autre l'innovation, la créativité, etc. Cette opposition ne pose-t-elle pas problème en soit ? N'est-ce pas là une vision qui vise à diviser ?}

[Page 39] Dans le cas de Kickstarter, on voit une corrélation entre le sujet ou le domaine des projets financés, et les centres d'intérêt des financeurs : par exemple un documentaire sur la première guerre mondiale aura du mal à trouver un financement via une plateforme de financement participatif.  
{Le "webcentrisme" ne favorise pas la diversité, mais une homogénéité propre à ce qu'internet a créé et crée pour le moment.}  
Autre danger : remplacer les institutions par des individus, et cela peut poser un certain nombre de problèmes pour le soutien d'un projet, la représentativité et la responsabilité.

[Page 40-41] Mais comment "définir les valeurs de la toile" ? Plusieurs exemples : Google et son esprit d'ouverture fantasmé, Wikipédia et sa reproductibilité imaginée, etc. Ce que l'on croit pour acquis – l'ouverture de Google ou l'invisibilité administrative de Wikipédia –, se révèle si l'on questionne mieux et l'on prend le temps de définir, avec "humilité". "Bien évidemment, le fait que quelque chose échappe à la grande théorie de 'la façon dont fonctionne internet' ne l'invalide pas, comme le montre bien l'exemple de Wikipédia."

#### Si les théoriciens d'internet étaient des videurs
[Page 42] Evgeny Morozov évoque la thèse de Jonathan Zittrain, "Elle part du principe que l'ouverture de la plateforme est la raison principale pour laquelle 'l'internet' a provoqué un tel déchaînement d'innovations." Mais internet n'est jamais remis en cause. {Evgeny Morozov se répète.}

{Dans le cas de la chaîne de publication que j'étudie, inspirée du web, l'idée est que certaines technologies sont intéressantes *maintenant*, dans un contexte particulier, et que cela pourrait ou même devrait forcément évoluer, sans reposer nécessairement sur le web. C'est simplement qu'actuellement, ce sont des solutions intéressantes.}

[Page 44-45] Le paradoxe souligné par Evgeny Morozov, selon lequel internet pourrait perdurer ou qu'au contraire il est ménacé et fragile, {je suis un peu perdu, j'ai l'impression qu'Evgeny Morozov tourne en rond...}

#### Époques et époqualismes
[Page 45] "Présenter le discours consacré à l'exception d'internet comme étant sans pareil serait une grossière erreur, car c'est tout sauf exact." Evgeny Morozov va donc consacrer cette partie à évoquer des penseurs qui ont conceptualisé de la même façon d'autres *révolutions* techniques ou technologiques, car internet semble provoquer une amnésie historique.

[Page 45-46] "Ce raisonnement erroné je le nomme 'epoqualisme'." Il s'agit autant des "optimistes" que des "pessimistes" : affirmer une révolution, quelque chose de totalement nouveau, sans l'expliquer ou le justifier, et surtout sans réellement comparer cela avec d'autres faits passés – convoquer l'histoire.

[Page 46] "Mais dans une vraie période de révolution, tout se vaut." L'"époqualisme" a ceci de pervers que le contexte de changement profond ne semble pas propice à une remise en cause. Bonus : un effet circulaire complètement fallacieux, qui fait qu'internet est révolutionnaire pour telle raison, et que cette même raison est révolutionnaire du fait d'internet. {Bullshit Bingo}

[Page 46-47] Evgeny Morozov prend plusieurs exemples passés de systèmes qui ont été repris avec internet : du crowdsourcing pour les cartes anglaises au dix-huitième siècle à la refonte d'un logo en 1936.

[Page 47] Globalement : les affirmations lancées par certains penseurs passent totalement à côté de la réalité, présente et passé. Par exemple les connaissances ne sont pas engendrées par internet, c'est simplement internet qui facilite ou décuple cela. "Webcentrisme".

[Page 48] Par exemple David Weinberger se trompe lorsqu'il dit que le savoir appartient dorénavant au réseau, cela a toujours été le cas, c'est même plutôt internet qui en est la conséquence.

#### Avec de tels modèles
[Pages 49-51] Le webcentrisme conduit certaines réflexions sur des bases qui sont elles-mêmes des généralisations. {En gros.}

[Page 51] Globalement ce que tente d'expliquer – avec quelques effets de répétition – Evgeny Morozov, c'est que le webcentrisme induit de tout vouloir expliquer au prisme d'internet, sans esprit critique. Conserver un esprit critique ne veut pas dire tout rejeter, bien au contraire.

[Page 52] Pour résumer : "Dans un pur esprit dialectique hégélien, le webcentrisme se maintient entre les deux pôles binaires que sont le pessimisme et l'optimisme d'internet, faisant passer toute critique à son encontre pour une manifestation supplémentaire de ces deux extrêmes." {Aussi appelé théorie du fer à cheval (les extrêmes se rencontrent) ? Un peu facile.}

#### Frénésie et conséquences
[Page 53] La stratégie des "théoriciens d'internet" est de ne pas utiliser de référence historique, c'est une pensée "anhistorique", parce qu'internet est suffisamment une "rupture" pour ne pas avoir à faire appel à ce qui s'est passé *avant*. Mais il y a eu d'autres "discours de la rupture", notamment avec le nucléaire ou l'électricité.

{En fait les "théoriciens d'internet" sont un peu trop enthousiastes, bornés, et mal cultivés.}

{Le risque en présentant une chaîne de publication inspriée du web comme étant une *révolution* serait de ne pas faire référence à d'autres expériences d'avant le web, et qui sont pourtant similaires.}

[Page 54-55] Si l'on souhaite poser les termes d'une révolution, il faut tenir compte de "deux critères" : il ne faut pas omettre ce qui s'est passé *avant*, et il faut s'appuyer sur le contexte contemporain global de cette supposée révolution. L'exemple des *digital natives* est très parlant : les "natifs du numérique" ont une mauvaise culture et maîtrise numérique et n'ont pas conscience des mécanismes sous-jacents des services et applications qu'ils utilisent.

#### Gutenberg dans le royaume du Geekhistan
[Page 56] Mais alors n'y a-t-il pas des changements notables avec internet ? Oui, mais le changement n'a pas été aussi brusque que l'on pourrait le penser, il n'y pas eu de réelle rupture, comme dans l'histoire de la pensée.

[Page 57-58] Evgeny Morozov fait de nouveau appel à Clay Shirky, qui a mis en place une dialectique à base de "flagellation" et de "bons conseils". Un exemple de la pensée de Clay Shirky est la comparaison entre "l'invention de l'imprimerie et l'avènement de 'l'internet'." Pourtant cela se passe dans un contexte fort différent, et finalement si l'on compare l'imprimerie et internet sous le prisme de Marshall Poe, c'est la même chose en mieux.

[Page 60] L'imprimerie et la figure de Gutenberg permet aux "geeks" de "donner un sens au présent ainsi qu'à l'avenir", donc donner de la profondeur à des événements liés à l'internet qui n'en ont peut-être pas... Christopher Kelty trouve même que la Réforme protestante fait office d'allégorie : "elle sépare le pouvoir du contrôle". La référence à Gutenberg est donc une façon de faire le récit d'une histoire contemporaine.

#### Une histoire bancale, de livres en blogs
[Page 60-61] Evgeny Morozov note que des penseurs comme Shirky et Jarvis font appel à Elizabeth Eisenstein pour expliquer la comparaison entre internet et l'imprimerie. Elizabeth Eisenstein "surestime" les qualités de la presse : fixité, facilité de diffusion et tendance à l'uniformisation. Mais le problème, pour Evgeny Morozov, c'est que la pensée d'Elizabeth Eisenstein elle-même a des limites, déjà soulignées par de nombreux universitaires : notamment parce qu'elle a tenté de marquer plus fortement une rupture qui existait mais qui n'était pas si *brusque*, par exemple avec la diffusion – limitée avec les manuscrits, mais possible avant l'arrivée des techniques mécaniques d'imprimerie.

[Page 62] Les critiques des universitaires sont finalement assez cohérentes : l'imprimerie n'est pas arrivée seule, elle est le fruit d'évolutions successives. "Ainsi, la version d'Eisenstein ne tient debout que que si l'on adhère à une séparation nette entre la technologie d'un côté, et la société et la culture de l'autre, et surtout si l'on accepte que ces dernières conditionnent ces premières, jamais l'inverse." Ce qu'influence l'imprimerie est aussi ce qui a permis son arrivée. Elle n'est pas arrivée de nulle part ! Comme le dit Johns elle n'est pas "sui generis" comme le pense Elizabeth Eisenstein.

{Une chaîne de publication inspirée du web ne marque pas une *rupture*, mais c'est une évolution qui prend appui sur différentes micro-évolutions, comme ce que décrit Gilbert Simondon avec les évolutions mineures et les évolutions majeures : l'histoire de la technique peut être brusque, mais jamais sans lien avec ce qui est passé.}

[Page 63] Elizabeth Eisenstein serait une figure de cette approche anhistorique et *sui generis* de la technologie, alors que l'on peut observer et analyser les évolutions de la technologie en relation avec son environnement, dont la société, en évitant le "médiacentrisme" : "centrisme" d'Elizabeth Eisenstein ou "webcentrisme" de Shirky. Deux points centraux : intégrer des "nuances" dans les analyses, et ne pas évincer les spécificités "locales majeures".

[Page 64] La seule ligne de défense de Shirky et Jarvis à l'égard des *scéptiques*, est une opposition binaire entre optimistes et pessimistes, les pessimistes étant "déconnectés du monde moderne". "La vision jarviso-eisenstenienne du monde estime que les outils sont figés. Ils sont extérieurs à la culture et à l'histoire."

#### Recycler le cycle
[Page 65] Deux choses : d'une part il y a l'inverse de la vision d'Elizabeth Eisenstein, qui complexifierait d'une certaine façon à la fois le passé et l'avenir {je ne comprends pas ce qu'Evgeny Morozov essaye de dire ici} ; d'autre part une *nouvelle* interprétation des évolutions technologiques pourrait être faite, façon téléologique.

[Page 66] Pour Evgeny Morozov on ne peut pas expliquer les évolutions historiques ou technologiques contemporaines à partir d'"anecdotes" et de "personnages" du passé : les interprétation du passé ne doivent pas devenir un principe pour l'avenir – exemple de la séparation entre l'état et les entreprises privées. {Une interprétation des propos d'Evgeny Morozov pourrait être qu'il ne souhaite pas remettre en cause le passé ou le présent, que ces deux choses sont immuables.}

[Page 67] On peut également noter un américano-centrisme : l'histoire ne se limite pas qu'aux États-Unis, exemple avec Malraux. "Il n'est pas nécessaire de voir le mal chez ces empereurs de l'information. Peut-être n'y a-t-il même rien à voir du tout."

[Page 68] "Le webcentrisme ne tolère aucune hypothèse contradictoire."

[Page 68-69] Evgeny Morozov insiste sur le fait que les gouvernements *peuvent* avoir un rôle important dans l'histoire des technologies, et qu'il ne s'agit pas de laisser toute liberté aux entreprises privées. Il ne faut pas avoir une vision binaire des choses. La stratégie de penseurs comme Elizabeth Eisenstein ou Tim Wu est la suivante :

1. une évolution technologique contemporaine est menacée
2. est-ce que dans le passé d'autres évolutions technologiques n'auraient pas subies des pressions similaires ?
3. retour dans le présent pour prouver que dans le passé il y a eu les mêmes menaces

[Page 69] "Le fait que ce webcentrisme nous coupe de la réalité est un motif d'inquiétude, non de réjouissances."

[Page 69] Conclusion de cette partie sur le "webcentrisme" : le webcentrisme est l'expression contemporaine et l'outil du solutionnisme. Il entraîne une absence de regard critique sur les technologies. Pire : il alimente la recherche d'efficacité, et déclenche des expérimentations basées sur celle-ci. Si le "webcentrisme" a d'une certaine façon aidé des combats militants, il a du même coup entrainé une perte de "clarté d'analyse" et de débat. Pour Evgeny Morozov le "webcentrisme" s'approche d'une religion, il faut donc revenir aux bases, oublier ce que nous savons, c'est l'objet des chapitres suivants.

### Chapitre 9 : Gadgets intelligents pour humains déficients
[Pages 310-312] Evgeny Morozov prend l'exemple d'un parcmètre "intelligent" qui permet de mesurer l'usage des places de parking, et y applique les principes d'Albert Hirschman : inanité, effet pervers et mise en péril. Il y a mise en péril puisque le système en lui-même est "défaillant" : il faudrait laisser le choix au conducteur concernant l'argent disponible dans le parcmètre à la fin de son stationnement, plutôt que de le remettre le compteur à zéro et ainsi d'alimenter les caisses de la ville de Santa Monica.  
"Il est possible, si nous généralisons ce projet et interdisons partout aux habitants de contrevenir à la loi, que nous finissions par générer des citoyens moralement déficients qui adopteront un bon comportement uniquement si l'infrastructure technologique les y contraint explicitement."

[Page 312] "ou être un bon concitoyen et aider plutôt ceux qui n'ont pas beaucoup de moyens à payer leur parking." {Ici Evgeny Morozov ne remet pas en question le fait que cet acte n'est pas forcément considéré comme créant de "bons concitoyens" par une Ville.}  
"Ainsi, même une légère modification dans le fonctionnement interne d'une chose aussi ordinaire qu'un système de stationnement pourrait produire des citoyens très différents." {La "légère modification" est autant le choix offert que la façon dont les utilisateurs vont pouvoir choisir. C'est donc aussi une question d'expérience utilisateur.}

[Page 313] Finalement ce projet présente trois risques – inanité, effet pervers et mise en péril – puisque l'objectif final n'est pas l'amélioration de la ville en cela qu'elle est l'amélioration des citoyens et de leur situation, mais uniquement les profits de la Ville.  
"Nous pouvons reporter l'analyse des questions quotidiennes apparemment insignifiantes tant qu'on le souhaite, mais en définitive elles reviendront toujours nous hanter."

#### Des trains victoriens et des huttes dans le Montana
{La réflexion d'Evgeny Morozov peut se résumer en : si vous décidez de trouver une solution à un problème, commencez à comprendre ce problème dans son ensemble, et non dans une partie comme la fin d'une chaîne. Il serait donc intéressant d'analyser une chaîne de publication inspirée du web avec cette lecture.}

[Page 314] Il ne suffit donc pas de relever des points d'un problème facilement résolvables, mais considérer le problème dans toute sa complexité, chercher à redéfinir les causes et les effets "en terme de relations, structures, et processus."  
"Le vrai problème avec le système de Santa Monica n'est pas qu'il soit intelligent, mais qu'il ne le soit pas suffisamment."  
{Je redouble ma critique exposée plus haut, Evgeny Morozov part du principe que le but des personnes et des structures est "de faire de nous des êtres plus réfléchis, plus bienveillants et plus humains", mais cela n'intéresse pas toutes les personnes et toutes les structures. Nous ne vivons pas dans un monde socialiste (indiquer ici des termes similaires).}

[Page 314-315] Là où nous nous trompons, c'est sur le fait de vouloir distinguer les solutions techniques et les questions éthiques/morales. Les deux sont forcément imbriquer. Et Evgeny Morozov d'évoquer Jacques Ellul comme grand défenseur de cette séparation de la "moralité" et de la "technologie".  
"Il est temps d'abandonner ce discours sur la 'Technologie' avec un *T* majuscule pour nous consacrer plutôt à découvrir de quelle manière d'autres technologies peuvent favoriser ou nuire à la condition humaine." {Et on retrouve ici la position de Gilbert Simondon : "la civilisation est mal technicienne".}

[Page 316] Mais Evgeny Morozov fait remarquer que le but n'est pas non plus de chercher une solution *technologique* à tout prix, parfois la technique n'est pas la solution {même si la politique est technique}.  
"Ainsi, dans bien des situations, les apports de la technologie pourraient s'avérer inévitables, ça n'est pas une raison pour crier 'Technopole !' et partir se retirer dans une hutte du Montana."  
Il s'agit donc de s'organiser pour chercher et trouver les meilleures solutions, sans vouloir rejeter la technologie par principe.

#### Des radios, des chenilles et des lampes
[Page 317] Il faut prendre en compte que les humains que nous sommes et le monde que nous habitons ne sont pas optimisables comme des machines : les solutions sont proportionnellement complexes au contexte des problèmes auxquelles elles répondent. La technologie peut aussi proposer un choix à l'humain qui l'utilise, et donc déplacer la solution : non plus dans un fonctionnement technique mais comme décision que va prendre un être conscient et réfléchissant. Evgeny Morozov prend l'exemple des "appareils erratiques".

{Une chaîne de publication inspirée ne doit pas être que fonctionnelle, elle peut également générer des interrogations et des réflexions de la part de ses utilisateurs. Exemples : certaines actions sont volontairement complexes, car elles nécessitent un niveau de décision et de concentration plus important ; à l'inverse écrire un texte ne doit pas devenir une tâche complexifiée comme avec un traitement de texte classique.}

[Page 318] On peut intégrer des dimensions autres dans les objets technologiques, proche d'une certaine forme de jugement ou de moralité, une sorte d'anthropomorphisme : la rallonge souffre si un appareil qui est branché sur elle est en veille. Il s'agit de provoquer une "réaction" chez l'utilisateur.

[Page 319] Evgeny Morozov parle de "friction" comme d'une "ressource productive", ou encore de "produit transformationnel" en évoquant les produits conçus par Caterpillar, cette entreprise utilise le terme de "perturbation" pour définir ce qu'elle conçoit et produit, plutôt que d'"amélioration".

[Page 320] "Ces utilisateurs sont tout sauf les robots de la théorie du choix rationnel ou de l'économie classique considérant chaque problème en fonction de principes immuables et bien définis et selon une compréhension parfaite de leurs courbes d'utilité."

#### Le Natural Fuse et ses adversaires
[Page 320] Evgeny Morozov définit le fonctionnalisme : les objets technologiques ont des fonctions et des objectifs, il s'agit alors de chercher à concevoir et produire des objets qui expriment cela le plus fortement possible. Le fonctionnalisme pousse les concepteurs, les designers, à ne pas penser des objets "initiateurs de débats".  
Il faut modifier les objectifs du fonctionnalisme : non plus chercher le meilleur et le plus efficace des objets technologiques pour effectuer telle tâche, mais que la relation entre la machine et son utilisateur provoque la solution, et c'est là le rôle du designer. {En d'autres termes, il faut que l'objet provoque une réflexion et ainsi résolve un problème. C'est bien dans cette relation avec l'utilisateur qu'un problème peut être résolu, et non en appuyant sur un bouton.}  
Il s'agit là de "design accusatoire".

[Pages 321-324] Evgeny Morozov présente la pensée de Carl DiSalvo et le projet Natural Fuse : habituellement le système gérant les taux d'émission de carbone permet de faire payer les pays qui en consomment d'autres, l'argent récolté permettant de faire fonctionner des puits de carbone (qui l'aspire). Ce système ne permet pas véritablement une prise de conscience et une action individuelle. Le projet Natural Fuse vise à représenter l'impact de nos consommations, via des plantes *branchées* et connectées en réseau : lorsque l'on consomme de l'énergie, la plante va petit à petit dépérir, l'utilisateur a la possibilité de récupérer l'énergie des autres plantes si les autres utilisateurs l'ont permis. Natural Fuse permet aux utilisateurs, aux "citoyens", de se poser la question de l'impact de leur consommation, *directement* et *concrètement*.  
Pour Carl DiSalvo il s'agit non pas d'utiliser le design pour concevoir un objet qui résolvera un problème, mais plutôt de permettre une situation qui provoquera une problématisation.

[Page 324] Pour Evgeny Morozov il faut passer d'une "conscience pratique" à une "conscience discursive" : la conscience pratique correspond à une mesure de nos consommations, à un rapport assez rationnel avec la technologie, alors que la conscience discursive correspond à une "introspection" à la fois personnelle et collective.

[Page 324-325] Natural Fuse est un exemple de projet utilisant la technologie, mais il provoque une réflexion, il nous pousse à remettre en cause des systèmes établis plutôt que de se contenter de se satisfaire d'un outil de mesure.

{Evgeny Morozov me donne le matériel critique nécessaire pour remettre en cause l'argument selon lequel les éditeurs n'auraient pas le temps de penser et concevoir leur outil de production.}

[Page 325-326] Evgeny Morozo prend de nouveaux exemples pour marquer le fait que, pour résoudre un problème, il faut plutôt problématiser plutôt que simplifier ou réduire.

#### Les fermes de contenus peuvent-elles être biologiques ?
[Page 327] Evgeny Morozov déplace ses questionnements depuis la consommation d'énergie vers celle de l'information : la problématique n'est pas tout à fait la même, cela pose plus de questions "ouvertes".

[Page 328] Pour résumer les interrogations posées par Evgeny Morozov : pourquoi ne pourrions-nous pas faire un Natural Fuse de l'information ? Sous-entendu : internet, en tant qu'il est désormais le vecteur le plus important de l'information, n'est-il pas trop fonctionnaliste, à travers les outils qui nous permettent de l'utiliser – car en soit internet n'est pas fonctionnaliste.  
Evgeny Morozov compare donc le fait de laisser un objet électrique en veille et le fait de consommer de l'information à outrance via de nombreux agrégateurs et fermes de contenus. {Pour l'instant cette comparaison me semble quelque peu bancale.}

[Page 329] Il est possible de rendre visible notre consommation d'information et son impact, alors pourquoi ne pas le faire ?  
"Les concepteurs et technologues devraient adopter l'idée que leurs objectifs ne se limitent pas à pousser les gens à utiliser leur appareil, mais également à les faire réfléchir avec eux."

#### La volonté en péril
[Page 329-330] Il ne faut plus faire appel à la psychologie à travers le solutionnisme, mais plutôt à la philosophie : moralité et prise de décision ne sont pas des *denrées* limitées comme le pétrole, mais plutôt des éléments qui s'auto-génèrent à mesure que l'on en crée.

[Page 331-332] Et le marketing contribue à chercher une solution immédiate et individualiste plutôt que de comprendre l'origine d'un problème et le résoudre collectivement. {C'est là le biais induit par la psychologie. D'une certaine façon le problème ne vient pas de l'utilisation de la psychologie mais plutôt d'une *mauvaise* utilisation de la psychologie.}

[Page 333] "L'existence des biais cognitifs ne devrait pas nous autoriser à ignorer les systèmes complexes qui arbitrent nos comportements." En d'autres termes ce n'est pas parce qu'il y a une prise de conscience du problème que l'on comprend ce problème et toute sa complexité.

[Page 333-334] La question du choix réel est centrale : on peut avoir l'impression de choisir – par exemple entre un produit de grande distribution et un produit bio –, alors que notre choix n'aura que peu d'incidence sur le global, et encore moins sur nous puisqu'il n'y aura plus de prise de conscience.

[Page 334-335] C'est le {soit-disant} choix rationnel qui nous pousse à chercher des solutions rapides plutôt qu'examiner le problème. {Evgeny Morozov tourne globalement en rond, même s'il est bon d'insister.} La recherche d'efficacité nous fait tout simplement perdre de vue le problème initial.

{Les traitements de texte et les logiciels de publication assistée par ordinateur sont clairement du côté du choix rationnel : la première contrainte est de résoudre un problème – concevoir et produire un livre, donc *publier* –, quand bien même ce problème n'est pas défini – en quoi consiste exactement *publier* ?}

#### Des pièges sans heurts
[Page 335] Le "solutionnisme" perdurera tant que l'être humain sera définit selon des "modèles simplistes". "[...] si l'on considère l'être lui-même comme étant contingent et en évolution constante, alors le processus par lequel il émerge compte autant que les actions qu'il produit." {Le parcours est aussi important le point d'arrivée.}

[Page 336-337] La sérénité, l'absence de conflit et l'absence de faille sont une influence de ceux qu'appellent Evgeny Morozov les "geeks" : la technique pouvant être optimisée continuellement, pourquoi ne pas faire la même chose avec l'humain ? L'exemple de la vie privée permet de comprendre que la contrainte peut être une bonne chose : "Le but de la vie privée n'est pas de protéger un soi stable de l'usure, mais de créer des frontières entre lesquelles il puisse jaillir, évoluer et se stabiliser. En d'autres termes, les limites et les contraintes peuvent être productives, bien que toute l'arrogance de 'l'internet' suggère le contraire."

[Page 338] La "technologie numérique" provoque un double effet : ouverture du spectre (on découvre plus de situations, différentes de la nôtre), et cloisonnement des expériences (la technologie a tendance à nous mettre à distance). À long terme la situation peut être encore pire qu'au départ... Et cela parce que certains de nos choix – Evgeny Morozov prend l'exemple de l'utilisation de moteurs de recherche qui ne respectent pas notre vie privée, ou auxquels nous *cédons* notre vie privée – ont un impact que l'on a du mal à percevoir.

[Page 339] Evgeny Morozov utilise deux termes pour définir la complexité de notre monde : la nature de l'être est "dynamique" et "émergente". Par ailleurs, nous construisons nos identités en nous observant nous-mêmes à travers le regard des autres, et cela n'est tout simplement pas possible avec des algorithmes qui ne nous donnent pas accès à ce qu'ils voient. La constitution de notre identité est donc grandement remise en cause, et la direction prise ne semble pas propice à faire de nous des personnes meilleures.

[Page 340] La perte d'autonomie ne semble pas problématique lorsque c'est pour une bonne cause – Evgeny Morozov prend l'exemple de l'économie d'énergie –, mais il y a pourtant un double "écœurement" : la "ruse" utilisée, et le fait que nous ayons l'impression d'avoir une autonomie.  
"Tenter d'améliorer la condition humaine en supposant tout d'abord que les personnes sont comme des robots ne nous mènera pas bien loin."

#### Technologies et vérités
[Page 341] La technique ne doit pas influencer nos comportements, cela peut être l'inverse : ce n'est pas parce que l'informatique n'est pas conçue actuellement pour oublier que l'on ne peut pas imaginer des processus de mémorisation plus proches du fonctionnement du cerveau humain.  
"La façon dont nos technologies numériques se développeront à l'avenir indiquera non pas la manière dont 'l'internet' ou les ordinateurs fonctionneront, mais comment nous choisirons de les faire fonctionner."

[Page 342] Il n'y a finalement pas grand chose de nouveau dans la plupart des phénomènes liés à "l'internet", nous pouvons donc savoir commence cela se passera sans attendre béatement. {Evgeny Morozov a lui-même tendance à simplifier les choses, et la grande force d'internet est plutôt la diffusion que la nouveauté ou l'innovation : diffusion d'information, et diffusion d'usage d'une certaine façon.}

[Page 342-343] Le principal défaut des "ingénieur" est de reconsidérer perpétuellement les situations, "Pour eux, tout est négociable, y compris la dignité et l'autonomie."  
Il faut réintroduire de l'irrationnalité, des débats, des délibérations, des questionnements.

[Page 343] "Il n'y a qu'en doutant résolument de lui-même que le solutionnisme pourra transcender ses limites intrinsèques."
