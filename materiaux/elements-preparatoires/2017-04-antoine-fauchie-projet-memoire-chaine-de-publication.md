---
layout: page-neutral
title: "Projet de mémoire en Master 2 Publication numérique (enssib)"
date: 2017-04-18
description: "Présentation du sujet, contexte, méthodologie, plan détaillé et références d'un projet de mémoire en Master 2 Publication numérique à l'École nationale supérieure des sciences de l'information et des bibliothèques."
---
### Antoine Fauchié, avril 2017
Présentation du sujet, contexte, méthodologie, plan détaillé et références d'un projet de mémoire en Master 2 Publication numérique à l'École nationale supérieure des sciences de l'information et des bibliothèques.


## Présentation du sujet

Les chaînes de publication ou chaînes d'édition sont ce par quoi le livre est conçu, écrit, édité. Interroger les modes de fabrication et de publication du livre, c'est questionner les outils et les pratiques d'écriture et d'édition.

Un livre prend aujourd'hui plusieurs formes et formats, physiques et numériques, et l'édition s'adapte à ces nouvelles contraintes liées à l'arrivée du numérique dans le domaine du livre. Différents types d'impression, plusieurs dimensions d'objets livre, des formats numériques variés : la fabrication du livre requiert désormais de nouvelles méthodes, tant en terme de conception – quelle lecture ? – que de production – quelle distribution et quelle diffusion seront permises ?

Après trente années de numérisation des outils de production de l'édition, les processus de fabrication des livres semblent maintenant partout identiques. Le duo traitement de texte et logiciel de publication assistée par ordinateur semble incontournable, et quelques logiciels ont une position hégémonique. Les outils de fabrication des livres, hors quelques pratiques marginales, deviennent des boîtes noires aux comportements incontrôlables et aux évolutions incontrôlées, posant la question de l'uniformisation des pratiques. Les maisons d'édition sont entrées en plein dans une conception solutionniste de la technique : la technologie doit résoudre un problème, quitte à ce que la maîtrise de ces outils disparaisse totalement.

L'édition doit relever de nouveaux défis liés à la gestion et à la fabrication des livres : prise en compte de modes collaboratifs d'écriture et interopérabilité des formats de travail. Les modes collaboratifs d'écriture et d'édition sont expérimentés depuis plusieurs années, notamment parce que beaucoup de maisons d'édition ont externalisé une partie des tâches liées à la correction et à la mise en forme des textes, et à la fabrication des livres. Les modes de lecture vont encore évoluer, nécessitant de nouvelles interventions sur des fichiers parfois vieux de plusieurs années : sans une interopérabilité des fichiers, et donc une indépendance vis-à-vis des logiciels utilisés par les éditeurs, les coûts et les efforts engendrés par ces manipulations seront gigantesques.

L'écosystème du web a déjà fait face à ces évolutions et ces défis : conception des contenus qui s'adaptent à différentes formes de consultation et de modes de diffusion, méthodes d'organisation pour des écritures collaboratives du code – et donc du texte –, standardisation des langages pour une interopérabilité et une certaine pérennité des contenus via leurs modes d'accès. Dès les premiers pas du développement web les professionnels du web ont mis en place des outils et des *workflows* leur permettant de se passer de logiciels.

Les chaînes de publication reposent majoritairement sur des logiciels fermés ou sur des systèmes figés, une évolution est nécessaire, notamment pour répondre aux nouveaux enjeux liés au numérique, mais comment l'aborder ? D'autres pratiques et techniques donneraient à l'édition l'opportunité de changer de paradigme, mais comment la saisir ? Les méthodes d'organisation et les technologies issues du développement web peuvent-elles permettre à l'édition de construire une chaîne de publication interopérable, modulaire et multiforme, et ainsi relever les défis imposés par le numérique ?


## Contexte

Ce mémoire s'inscrit dans le Master 2 Publication numérique de l'École nationale supérieur des sciences de l'information et des bibliothèques (Villeurbanne), dans le domaine des sciences de l'information : [http://www.enssib.fr/master2-publication-numerique-lyon](http://www.enssib.fr/master2-publication-numerique-lyon).

Le sujet de ce mémoire s'appuie sur plusieurs observations et sur une intuition : plusieurs expérimentations récentes de chaînes de publication reposent sur des méthodes et des technologies issues du web ; ces méthodes et technologies pourraient tendre à remplacer des logiciels dont les éditeurs n'ont plus la maîtrise. Pour présenter ces observations et cette intuition, j'ai publié en mars 2017 un article intitulé [Une chaîne de publication inspirée du web](https://www.quaternum.net/2017/03/13/une-chaine-de-publication-inspiree-du-web/).

Les questions liées aux chaînes de publication sont rarement abordées, trop souvent divisées entre les sciences de l'information et les questions de design. Ce mémoire sera l'occasion d'interroger des pratiques métier dans une situation de révolution des pratiques et des usages, et dans un environnement encore en pleine mutation.

Ce document présente un projet de mémoire, cet énoncé devra nécessairement gagner en profondeur théorique et ne pas se cantonner à un terrain pratique.

Deux travaux de recherche sont en lien direct avec ce sujet : Chloé Girard écrit une thèse intitulée "Fixation des états du texte en SHS" ([plus d'informations](http://doctorat.edition.international/doku.php)), et Julie Blanc est en train de définir son sujet de thèse "Pour un design des systèmes de publications contemporains et leur écoconception" ([plus d'informations](http://julie-blanc.fr/slide/these-mars2017/index.html)).


## Méthodologie

Des phases de travail précises, un état de l'art, une série d'entretiens, un travail d'analyse et une liste de contraintes en rapport avec le sujet du mémoire constituent la méthodologie utilisée.

### Phases de travail
Voici les différentes phases de travail envisagées pour la réalisation de ce mémoire :

* avril 2017 : définition du sujet et sollicitation de directeurs de mémoire ;
* avril-mai 2017 : lectures, enrichissement du sujet avec des éléments théoriques, état de l'art ;
* avril-mai 2017 : précision du plan détaillé, présentation de l'état de l'art ;
* avril-juillet 2017 : réalisation des entretiens, participation à des workshops, interventions sur le sujet, rédaction d'articles en parallèle ;
* juin-septembre 2017 : rédaction du mémoire ;
* avril-septembre 2017 : expérimentations en rapport avec le sujet ;
* octobre 2017 : dépôt du mémoire et soutenance.


### État de l'art
Travail de recherche en rapport avec le sujet : cas pratiques pouvant alimenter et illustrer l'hypothèse, ouvrages et articles théoriques permettant de prendre le recul nécessaire, événements passés ou à venir en lien avec le sujet.


### Série d'entretiens
Un entretien avec un professionnel sera intégré à chaque sous-partie, soit 9 entretiens en tout. Éditeurs, designers, graphistes, développeurs, auteurs seront interrogés sur leurs pratiques d'édition et de publication, ils seront sélectionnés selon plusieurs critères : lien ou absence de lien avec le livre numérique, expérimentation de méthodes et d'outils alternatifs ou utilisation de logiciels classiques, changement de pratiques en lien avec les chaîne de publication.

Ces échanges étayeront ou critiqueront mes propos, et seront également le moyen d'expliciter des suppositions via des cas pratiques en plus des articles qui abordent des retours d'expérience.


### Contraintes
Pour la réalisation de ce mémoire voici une liste de contraintes, elles sont parties intégrantes de la méthodologie :

* mode de fabrication : les méthodologies et les technologies présentées dans ce mémoire seront utilisées pour les différentes phases, il s'agit de techniques et de pratiques issues du web. Cela inclut : l'utilisation d'un langage sémantique comme Markdown ou Asccidoc, une méthode de versionnement comme git, la production et la publication du mémoire sous différentes formes et différents formats, la documentation des processus de développement, etc. ;
* la collaboration et la révision sera donc possible pendant et après le mémoire, un dépôt git privé a d'ores et déjà été créé ;
* les contenus disponibles sur le dépôt git sont encore à préciser : références, résumés d'articles et d'ouvrages, comptes rendus de conférences et de workshops, présentation d'expérimentations, chapitres du mémoire, tableau de bord des tâches à réaliser, etc.


## Plan détaillé
Voici le plan détaillé de ce mémoire consacré aux modes de conception et de fabrication des livres :

### Introduction
Reformulation de l'énoncé, problématisation, annonce du plan, présentation des enjeux.


### 1. Les chaînes de publication à l'ère numérique
Chronologie des modifications des outils et des pratiques.

#### 1.1. Arrivée des premiers outils informatiques
Rappels concernant l'histoire de l'édition sous l'axe de la conception/fabrication des livres.  
Histoire de la rencontre de l'édition avec l'informatique.  
Le livre numérique et ses contraintes : une phase déterminante dans la rencontre du numérique et de l'édition.

#### 1.2. Le duo traitement de texte et logiciel de PAO
Description de chaînes de publication *classiques*.  
L'usage de deux logiciels, et pourquoi cette hégémonie.  
Des approches alternatives : méthodes et outils.

#### 1.3. Trois alternatives
Présentation de LaTeX, Docbook (O'Reilly) et de chaînes XML.  
L'approche sémantique : séparation de la structure et de la mise en forme.  
Limites de ces trois exemples : nouvelles dépendances, difficultés d'utilisation, complexité, dette technique.

*Articulation : s'appuyer sur ces alternatives pour penser un nouveau modèle, et pas uniquement remplacer des outils par d'autres outils.*


### 2. L'influence des technologies et organisations du web sur les chaînes de publication
Analyse de nouvelles approches et de nouveaux modes de fonctionnement.

#### 2.1. Point sur les technologies et les organisations du web
Des standards de structuration et de mise en forme pour une diffusion massive.  
Mise en place de modes collaboratifs : *versioning*.  
Enjeux techniques, politiques et sociaux de ces méthodes d'organisation et de ces technologies. + accessibilité

#### 2.2. Une chaîne de publication inspirée du web
Présentation d'un modèle qui s'appuie sur des méthodes et technologies issues du web : The Getty Publications.  
Comment la chaîne de publication est repensée ? Comparatif avec des chaînes classiques.  
Les étapes détaillées d'une chaîne de publication.

#### 2.3. Trois principes : interopérabilité, modularité, multiforme
Présentation (théorique) des trois principes.  
Application de solutions techniques/pratiques à ces principes.  
Nouvelle lecture d'une chaîne de publication avec ces principes, et implications.

*Articulation : trois principes (théoriques) mais une réalité, comment appliquer cela ?*


### 3. Quels défis ?
À partir des éléments énoncés dans la partie 2, quels défis se présentent pour la mise en place et l'appréhension d'une telle chaîne ?

#### 3.1. Les contraintes formelles et de production
Limites du modèle HTML+CSS, pistes d'évolution (CSS region, etc.).  
Complexité et puissance de git, quelques exemples pratiques.  
Les questions de l'évolution d'une telle chaîne de publication (long terme).

#### 3.2. Faciliter la prise en main d'outils complexes
Reprendre les étapes : écrire, collaborer, éditer, etc.  
La question des APIs.  
Les chantiers encore nécessaires : éditeurs de texte, utilisation de git, etc.

#### 3.3. Réflexions au-delà des chaînes de publication
*Peut-être que ce n'est pas une bonne idée d'aborder ce point en fin de mémoire, à réserver pour la conclusion ?*


### Conclusion
Synthèse des conclusions des trois parties, parler plus de méthodologie que de technologie. Ouverture vers : la question de l'évolution des pratiques et l'urgence de cette évolution, et d'autres situations (autre que la publication) où ces principes pourraient s'appliquer.


### Mots-clés, termes du mémoire

Voici une liste de mots-clés du mémoire, il s'agit des termes privilégiés : édition, chaîne d'édition, chaîne de publication, livre, outils, écriture, lecture, publication, livre numérique, logiciels, sémantique, structure, web, développements web, etc.


### Références

Les références précédées d'une étoile sont en cours de sélection – donc peut-être pas pertinentes – et/ou en cours de lecture.

#### Ouvrages

ATTWELL Arthur, *[The Electric Book workflow](http://electricbook.works/)*, 2016.

`*` BJARNASON Baldur et ABBA Tom, *[Writing in the age of the web](http://thisisnotabook.baldurbjarnason.com//)*, 2015.

`*` BON François, *Après le livre*, Paris, Seuil, 2011.

COLLECTIF, *Lire à l’écran. Contribution du design aux pratiques et aux apprentissages des savoirs dans la culture numérique*, ésad Grenoble-Valence, B42, 2011.

`*` COLLECTIF, *Publier, éditer, éditorialiser. Nouveaux enjeux de la production numérique*, Louvain-la-Neuve, De Boeck Supérieur, ADBS, 2016.

COLLECTIF, *Read-write book : le livre inscriptible*, Marseille, Centre pour l'édition électronique ouverte, 2010.

DEMAREE David, *Git for Humans*, New York, A Book Apart, 2016.

EBERLE-SINATRA Michael, VITALI-ROSATI Marcello, *Pratiques de l’édition numérique*, Montréal, Les Presses de l'Université de Montréal, 2014.

JAILLOT Bastien, *La dette technique*, Périgneux, le Train de 13h37, 2015.

KEITH Jeremy, *[Resilient Web Design](https://resilientwebdesign.com/)*, 2016.

LUDOVICO Alessandro, *Post-Digital Print. La mutation de l'édition depuis 1894*, Paris, B42, 2016.

MARCOTTE Ethan, *Responsive Web Design*, Paris, Groupe Eyrolles, 2017.

ROSSET Christian et GERNER Jochen, *Le minimalisme*, Bruxelles, Le Lombard, 2016.

T. PÉDAUQUE Roger, *Le Document à la lumière du numérique*, Caen, C&F Éditions, 2006.


#### Articles

ANDREW Rachel, ["Designing For Print With CSS"](https://www.smashingmagazine.com/2015/01/designing-for-print-with-css/), *Smashing Magazine*, janvier 2015.

CRAMER Dave, ["Beyond XML: Making Books with HTML"](https://www.xml.com/articles/2017/02/20/beyond-xml-making-books-html/), *XML.com*, 20 février 2017.

FAUCHIÉ Antoine, ["Écrire un livre en 2017"](https://www.quaternum.net/2017/03/07/ecrire-un-livre-en-2017/), *quaternum.net*.

FAUCHIÉ Antoine, GARDNER Eric, ["Publier des livres avec un générateur de site statique"](https://jamstatic.fr/2017/01/23/produire-des-livres-avec-le-statique/), *JAMStatic*.

FÉTRO Sophie, [Œuvrer avec les machines numériques"](http://www.revue-backoffice.com/numeros/01-faire-avec/sophie-fetro-oeuvrer-machines-numeriques), *Back Office*, n°1, 2017.

GARDNER Eric, ["Digital Publishing Needs New Tools"](http://blogs.getty.edu/iris/digital-publishing-needs-new-tools/), *The Getty Iris*.

GIRARD Chloé, ["Livre liquide/livre solide."](http://ecultures.hypotheses.org/160), *Cultures numériques*, juillet 2014.

HOGAN Lara, ["Coding a book"](http://larahogan.me/blog/coding-a-book/).

iA, ["Multichannel Text Processing"](https://ia.net/topics/multichannel-text-processing/).

LANE Ruth Evans, ["An Editor’s View of Digital Publishing"](http://blogs.getty.edu/iris/an-editors-view-of-digital-publishing/), *The Getty Iris*.

MAUDET Nolwenn, ["Muriel Cooper, Information Landscapes"](http://www.revue-backoffice.com/numeros/01-faire-avec/nolwenn-maudet-muriel-cooper-information-landscapes), *Back Office*, n°1, 2017.

MCKESSON Nellie, ["Building Books with CSS3"](https://alistapart.com/article/building-books-with-css3), *A List Apart*, 12 juin 2012.

MULLER Catherine, ["Retour sur la Biennale du numérique 2015. Métiers du livre (2/4) : édition XML-TEI, écriture et document numérique"](http://www.enssib.fr/recherche/enssiblab/les-billets-denssiblab/biennale-du-numerique-edition-numerique-encodage-xml-tei), *Les billets d’enssibLab*.

OSP, ["HTML sauce cocktail, sauce à part"](http://ospublish.constantvzw.org/blog/news/html-sauce-cocktail-sauce-a-part), OSP-BLOG.

SUNDAR Stacey, ["Everything About E-Publishing"](https://medium.com/type-thursday/everything-about-e-publishing-dee31b059fb), *TypeThursday*, 25 mars 2017.

TAILLANDIER Frank, ["La mouvance statique"](https://frank.taillandier.me/2016/03/08/les-gestionnaires-de-contenu-statique/).

TAQUET Julien, ["Making books with HTML + CSS — a look at Vivliostyle (Part 1 – page layouts)"](https://www.pagedmedia.org/making-book-with-html-css-a-look-at-vivliostyle-part-1-page-layouts/), *Paged Media*.

VITALI-ROSATI Marcello, ["Qu'est-ce que l'éditorialisation ?"](http://sens-public.org/article1184.html), *Sens Public*, mars 2016.


#### Autres

GRIFFIN Matt, *[What Comes Next Is The Future](http://www.futureisnext.com/)*, documentaire, 2016.

PrePostPrint, [http://prepostprint.org/](http://prepostprint.org/), groupe à géométrie variable et workshops irréguliers, depuis avril 2017.